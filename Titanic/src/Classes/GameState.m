//
//  GameState.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "GameState.h"

@implementation GameState


- (id)initWithWidth:(float)theWidth andHeight:(float)theHeight
{
    self = [super init];
    if (self) {
        self.width = theWidth;
        self.height = theHeight;
        self.pivotX = 0;
        self.pivotY = theHeight;
        self.x = 0;
        self.y = 0;
    }
    return self;
}

- (void)stateWillShow {
    
}

- (void)stateDidHide {
    
}

@end
