//
//  GameState.h
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"

@interface GameState : SPSprite

- (id)initWithWidth:(float)theWidth andHeight:(float)theHeight;
- (void)stateWillShow;
- (void)stateDidHide;


@end