//
//  PauseView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "PauseView.h"
#import "GameNotifications.h"

#define PAUSE_BUTTONS_POS_Y 220
#define PAUSE_BUTTONS_OFFSET 130

@interface PauseView()
-(void)onResumeButtonTriggered:(SPEvent *)event;
-(void)onRetryButtonTriggered:(SPEvent *)event;
-(void)onQuitButtonTriggered:(SPEvent *)event;
-(void)onPlayButtonTriggered:(SPEvent *)event;
@end

@implementation PauseView

- (id)init
{
    self = [super init];
    if (self) {
            }
    return self;
}

- (void)dealloc
{
    [resumeButton release];
    [retryButton release];
    [quitButton release];
    [playButton release];
    [pageImage release];
    [background release];
    [super dealloc];
}


#pragma mark - Trigger Events

- (void)onResumeButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyResumeLevel object:nil];     
}

- (void)onQuitButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyQuitLevel object:nil];    
}

- (void)onRetryButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyRetryLevel object:nil];    
}

- (void)onPlayButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyResumeLevel object:nil];     
}

#pragma mark - View Management

- (void)viewWillShow {
    if (background == nil) {
        SPTextureAtlas *playItemsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_items.xml"];
        SPTextureAtlas *playButtonsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_buttons.xml"];
        
        float screenWidth = [UIScreen mainScreen].bounds.size.height;
        float screenHeight = [UIScreen mainScreen].bounds.size.width;
        
        background = [[SPQuad alloc] initWithWidth:screenWidth height:screenHeight color:0x000000];
        background.alpha = 0.8;
        
        pageImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"pausemenu"]];
        pageImage.x = (screenWidth - pageImage.width) / 2;
        pageImage.y = 50;
        
        resumeButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"resumeButton"] downState:[playButtonsAtlas textureByName:@"resumeButton_down"]];
        resumeButton.x = (screenWidth / 2) - (resumeButton.width / 2) - PAUSE_BUTTONS_OFFSET;
        resumeButton.y = PAUSE_BUTTONS_POS_Y;
        
        retryButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"tryitagainButton"] downState:[playButtonsAtlas textureByName:@"tryitagainButton_down"]];
        retryButton.x = (screenWidth / 2) - (retryButton.width / 2);
        retryButton.y = PAUSE_BUTTONS_POS_Y;
        
        quitButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"quitButton"] downState:[playButtonsAtlas textureByName:@"quitButton_down"]];
        quitButton.x = (screenWidth / 2) - (quitButton.width / 2) + PAUSE_BUTTONS_OFFSET;
        quitButton.y = PAUSE_BUTTONS_POS_Y;
        
        playButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"playButton"] downState:[playButtonsAtlas textureByName:@"playButton_down"]];
        
        // ADD CHILDS
        [self addChild:background];
        [self addChild:pageImage];
        [self addChild:resumeButton];
        [self addChild:retryButton];
        [self addChild:quitButton];
        [self addChild:playButton];
        
        // EVENT LISTENERS
        [resumeButton addEventListener:@selector(onResumeButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [retryButton addEventListener:@selector(onRetryButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [quitButton addEventListener:@selector(onQuitButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [playButton addEventListener:@selector(onPlayButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];

        [playItemsAtlas release];
        [playButtonsAtlas release];
    }
}

- (void)viewDidHide {
    [self removeAllChildren];
    [resumeButton release];
    resumeButton = nil;
    [retryButton release];
    retryButton = nil;
    [quitButton release];
    quitButton = nil;
    [playButton release];
    playButton = nil;
    [pageImage release];
    pageImage = nil;
    [background release];
    background = nil;
}


@end
