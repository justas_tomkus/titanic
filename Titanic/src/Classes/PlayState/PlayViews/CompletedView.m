//
//  CompletedView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "CompletedView.h"
#import "GameNotifications.h"
#import "Level.h"
#import "StorageTool.h"

#define BUTTON_OFFSET 50
#define TEXTS_LEFT_POS_X 50

@interface CompletedView()
-(void)onRetryButtonTriggered:(SPEvent *)event;
-(void)onNextChamberButtonTriggered:(SPEvent *)event;
-(void)onQuitButtonTriggered:(SPEvent *)event;
-(void)resetView;
-(void)renderView;
@end

@implementation CompletedView

@synthesize levelNumber;
@synthesize retryButton;
@synthesize nextButton;
@synthesize quitButton;

- (id)init {
    self = [super init];
    if (self) {
                        
    }
    return self;
}

- (void)dealloc
{    
    [mainWonTexts release];
    [mainLostTexts release];
    
    [overlay release];
    [backgroundImage release];
    [pageImage release];
    [mainText release];
    [playingTimeText release];
    [dustCountText release];
    [lostMakkusText release];
    [scoreText release];
    [nextButton release];
    [retryButton release];
    [quitButton release];
    [super dealloc];
}


- (void)loadForLevel:(int)lvlNumber andSuccess:(BOOL)theSuccess {
    self.levelNumber = lvlNumber;
    levelSuccessful = theSuccess;
    
    [self resetView];
    [self renderView];
}


#pragma mark - Private Methods

- (void)resetView {
    [self removeAllChildren];
}

- (void)renderView {
    NSDictionary *levelData = [StorageTool getScoreForLevel:levelNumber];
    
    int minutes = floor([[levelData objectForKey:@"timeInSeconds"] intValue] / 60.0);
    int seconds = [[levelData objectForKey:@"timeInSeconds"] intValue] - (minutes * 60);
    SPTextureAtlas *playItemsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_items.xml"];
    
    NSString *minutesString = (minutes == 0) ? @"0" : [NSString stringWithFormat:@"%d",seconds];
    NSString *secondsString = (seconds < 10) ? [NSString stringWithFormat:@"0%d",seconds] : [NSString stringWithFormat:@"%d",seconds];
    
    playingTimeText.text = [NSString stringWithFormat:@"Your time: %d:%d", minutesString, secondsString];                                                                         
    dustCountText.text = [NSString stringWithFormat:@"Dust gathered: %d / %d", [[levelData objectForKey:@"collectedDust"] intValue], [[levelData objectForKey:@"maximumDust"] intValue]];
    lostMakkusText.text = [NSString stringWithFormat:@"Makkus lost: %d", 12];
    scoreText.text = [NSString stringWithFormat:@"%d", [[levelData objectForKey:@"score"] intValue]];

    if (!levelSuccessful) {
        mainText.text = [mainWonTexts objectAtIndex:levelNumber-1];
    
        backgroundImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"levelStory_background2"]];
        pageImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"YouLostScreen"]];
        
        [retryButton addEventListener:@selector(onRetryButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    } else {
        mainText.text = [mainLostTexts objectAtIndex:levelNumber-1];
        
        backgroundImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"levelStory_background"]];
        pageImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"YouWonScreen"]];
        
        [nextButton addEventListener:@selector(onNextChamberButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    }
    
    [playItemsAtlas release];
    
    backgroundImage.x = (screenWidth - backgroundImage.width) / 2;
    backgroundImage.y = 50;
    
    pageImage.x = 0;
    pageImage.y = 15;
    
    scoreText.x = (screenWidth / 2) - (scoreText.width / 2);
    
    
    [quitButton addEventListener:@selector(onQuitButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    
    [self addChild:overlay];    
    [self addChild:backgroundImage];
    [self addChild:pageImage];
    [self addChild:mainText];
    [self addChild:playingTimeText];
    [self addChild:dustCountText];
    [self addChild:scoreText];
    [self addChild:quitButton];

    if (!levelSuccessful) {
        [self addChild:retryButton];
    }else {
        [self addChild:nextButton];
    }
}


#pragma mark - Trigger Events

-(void)onRetryButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyRetryLevel object:nil]; 
}

-(void)onNextChamberButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyNextLevel object:nil];    
}

-(void)onQuitButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyQuitLevel object:nil];    
}

#pragma mark - View Management

- (void)viewWillShow {
    if (overlay == nil) {
        SPTextureAtlas *playItemsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_items.xml"];
        SPTextureAtlas *playButtonsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_buttons.xml"];
        
        screenWidth = [UIScreen mainScreen].bounds.size.height;
        screenHeight = [UIScreen mainScreen].bounds.size.width;
        
        overlay = [[SPQuad alloc] initWithWidth:screenWidth height:screenHeight color:0x000000];
        overlay.alpha = 0.8;
        
        
        // TEXTS
        NSString *mainWon1 = @"Level 1 succeeded!";
        NSString *mainWon2 = @"Level 2 succeeded!";
        mainWonTexts = [[NSMutableArray alloc] initWithObjects:mainWon1,mainWon2, nil];
        
        NSString *mainLost1 = @"Level 1 succeeded!";
        NSString *mainLost2 = @"Level 2 succeeded!";
        mainLostTexts = [[NSMutableArray alloc] initWithObjects:mainLost1,mainLost2, nil];
        
        mainText = [[SPTextField alloc] initWithText:@"Success!"];
        mainText.x = TEXTS_LEFT_POS_X;
        mainText.y = 100;
        mainText.height = 150;
        mainText.fontName = @"Chalkduster";
        mainText.fontSize = 16;
        mainText.vAlign = SPVAlignTop;
        
        playingTimeText = [[SPTextField alloc] initWithText:@""];
        playingTimeText.x = TEXTS_LEFT_POS_X;
        playingTimeText.y = 110;
        playingTimeText.width = 150;
        playingTimeText.fontName = @"Chalkduster";
        playingTimeText.fontSize = 12;
        playingTimeText.hAlign = SPHAlignLeft;
        
        dustCountText = [[SPTextField alloc] initWithText:@""];
        dustCountText.x = TEXTS_LEFT_POS_X;
        dustCountText.y = 120;
        dustCountText.fontName = @"Chalkduster";
        dustCountText.fontSize = 12;
        dustCountText.hAlign = SPHAlignLeft;
        
        lostMakkusText = [[SPTextField alloc] initWithText:@""];
        lostMakkusText.x = TEXTS_LEFT_POS_X;
        lostMakkusText.y = 140;
        lostMakkusText.fontName = @"Chalkduster";
        lostMakkusText.fontSize = 12;
        lostMakkusText.hAlign = SPHAlignLeft;
        
        scoreText = [[SPTextField alloc] initWithText:@""];
        scoreText.x = TEXTS_LEFT_POS_X;
        scoreText.y = 150;
        scoreText.fontName = @"Chalkduster";
        scoreText.fontSize = 24;
        scoreText.hAlign = SPHAlignCenter;
        
        
        // BUTTONS
        quitButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"quitButton"] downState:[playButtonsAtlas textureByName:@"quitButton_down"]];
        quitButton.x = (screenWidth / 2) - (quitButton.width / 2) - BUTTON_OFFSET;
        quitButton.y = 260;
        
        retryButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"tryitagainButton"] downState:[playButtonsAtlas textureByName:@"tryitagainButton_down"]];
        retryButton.x = (screenWidth / 2) - (retryButton.width / 2) + BUTTON_OFFSET;
        retryButton.y = 260;
        
        nextButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"nextButton"] downState:[playButtonsAtlas textureByName:@"nextButton_down"]];        
        nextButton.x = (screenWidth / 2) - (nextButton.width / 2) + BUTTON_OFFSET;
        nextButton.y = 260;
        
        // RELEASES
        [playButtonsAtlas release];
        [playItemsAtlas release];
    }
}

- (void)viewDidHide {
    [self removeAllChildren];
    
    [mainWonTexts release];
    mainWonTexts = nil;
    [mainLostTexts release];
    mainLostTexts = nil;
    
    [overlay release];
    overlay = nil;
    [backgroundImage release];
    backgroundImage = nil;
    [pageImage release];
    pageImage = nil;
    [mainText release];
    mainText = nil;
    [playingTimeText release];
    playingTimeText = nil;
    [dustCountText release];
    dustCountText = nil;
    [lostMakkusText release];
    lostMakkusText = nil;
    [scoreText release];
    scoreText = nil;
    [nextButton release];
    nextButton = nil;
    [retryButton release];
    retryButton = nil;
    [quitButton release];
    quitButton = nil;
}

@end
