//
//  LostView.h
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"
#import "ViewManagement.h"

@interface LostView : SPSprite <ViewManagement> {
    SPButton *retryButton;
    SPButton *quitButton;
    SPImage *pageImage;
    SPQuad *background;
}

@end
