//
//  IntroView.h
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"
#import "ViewManagement.h"

@interface IntroView : SPSprite <ViewManagement> {
    int level;
    BOOL showStory;
    
    NSMutableArray *storyTextes;
    NSMutableArray *levelMainTextes;
    NSMutableArray *levelQuestTextes;
    
    SPTextureAtlas *chamberAtlas;
    SPQuad *overlay;
    SPImage *backgroundImage;
    SPImage *storyImage;
    SPTextField *mainText;
    SPTextField *questText;
    
    int curPageIndex;
}

@property (nonatomic, strong) SPButton *nextButton;
@property (nonatomic, strong) SPButton *startButton;
@property (nonatomic, strong) SPButton *skipButton;
@property (nonatomic, strong) SPImage *levelImage;

- (void)loadForLevel:(int)theLevel withStory:(BOOL)theShowStory;

@end
