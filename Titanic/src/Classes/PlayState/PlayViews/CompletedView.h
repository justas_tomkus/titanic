//
//  CompletedView.h
//  Titanic
//
//  Created by Philipp Anger on 24.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"
#import "ViewManagement.h"



@interface CompletedView : SPSprite <ViewManagement> {

    BOOL levelSuccessful;
    
    NSMutableArray *mainWonTexts;
    NSMutableArray *mainLostTexts;
    
    float screenWidth;
    float screenHeight;
    
    SPQuad *overlay;
    SPImage *backgroundImage;
    SPImage *pageImage;
    SPTextField *mainText;
    SPTextField *playingTimeText;
    SPTextField *dustCountText;
    SPTextField *lostMakkusText;
    SPTextField *scoreText;
}

@property (nonatomic, assign) int levelNumber;
@property (nonatomic, strong) SPButton *nextButton;
@property (nonatomic, strong) SPButton *retryButton;
@property (nonatomic, strong) SPButton *quitButton;

- (void)loadForLevel:(int)lvlNumber andSuccess:(BOOL)theSuccess;

@end
