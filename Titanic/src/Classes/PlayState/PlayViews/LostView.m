//
//  LostView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "LostView.h"
#import "GameNotifications.h"

#define LOST_BUTTONS_POS_Y 220
#define LOST_BUTTONS_OFFSET 130

@interface LostView()
-(void)onRetryButtonTriggered:(SPEvent *)event;
-(void)onQuitButtonTriggered:(SPEvent *)event;
@end

@implementation LostView

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    [retryButton release];
    [quitButton release];
    [pageImage release];
    [background release];
    [super dealloc];
}


#pragma mark - Trigger Events

- (void)onRetryButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyRetryLevel object:nil];     
}

- (void)onQuitButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyQuitLevel object:nil];    
}

#pragma mark - View Management

- (void)viewWillShow {
    if (background == nil) {
        SPTextureAtlas *playItemsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_items.xml"];
        SPTextureAtlas *playButtonsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_buttons.xml"];
        
        float screenWidth = [UIScreen mainScreen].bounds.size.height;
        float screenHeight = [UIScreen mainScreen].bounds.size.width;
        
        background = [[SPQuad alloc] initWithWidth:screenWidth height:screenHeight color:0x000000];
        background.alpha = 0.8;
        
        pageImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"youlost"]];
        pageImage.x = (screenWidth - pageImage.width) / 2 - LOST_BUTTONS_OFFSET;
        pageImage.y = LOST_BUTTONS_POS_Y;
        
        retryButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"tryitagainButton"] downState:[playButtonsAtlas textureByName:@"tryitagainButton_down"]];
        retryButton.x = (screenWidth / 2) - (retryButton.width / 2);
        retryButton.y = LOST_BUTTONS_POS_Y;
        
        quitButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"quitButton"] downState:[playButtonsAtlas textureByName:@"quitButton_down"]];
        quitButton.x = (screenWidth / 2) - (quitButton.width / 2) + LOST_BUTTONS_OFFSET;
        quitButton.y = LOST_BUTTONS_POS_Y;
        
        
        // ADD CHILDS
        [self addChild:background];
        [self addChild:pageImage];
        [self addChild:retryButton];
        [self addChild:quitButton];
        
        [playItemsAtlas release];
        [playButtonsAtlas release];
        
        // EVENT LISTENERS
        [retryButton addEventListener:@selector(onRetryButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [quitButton addEventListener:@selector(onQuitButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    }
}

- (void)viewDidHide {
    [self removeAllChildren];
    [background release];
    background = nil;
    [pageImage release];
    pageImage = nil;
    [retryButton release];
    retryButton = nil;
    [quitButton release];
    quitButton = nil;
}

@end
