//
//  HUDView.h
//  Titanic
//
//  Created by Philipp Anger on 11.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"

@interface HUDView : SPSprite {
    SPButton *pauseButton;
    
    SPQuad *dustBarQuad;
    SPImage *dustBarLimit;
    SPImage *arrow;
    float screenWidth, screenHeight;
    float arrowDistanceRadius;
}

- (void)updateDustBarForMaximum:(float)maximum andValue:(float)value andLimit:(float)limit;
- (void)showArrowWithVector:(CGPoint) scaledNormal;
- (void)hideArrow;
@end
