//
//  GameFinishedView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "GameFinishedView.h"
#import "GameNotifications.h"
#import "StorageTool.h"

#define BUTTON_OFFSET 50
#define TEXTS_LEFT_POS_X 50

@interface GameFinishedView()
-(void)onMenuButtonTriggered:(SPEvent *)event;
@end

@implementation GameFinishedView

@synthesize menuButton;
- (id)init {
    self = [super init];
    if (self) {
        SPTextureAtlas *playItemsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_items.xml"];
        SPTextureAtlas *playButtonsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_buttons.xml"];
        
        float screenWidth = [UIScreen mainScreen].bounds.size.height;
        float screenHeight = [UIScreen mainScreen].bounds.size.width;
        
        NSDictionary *levelScores = [StorageTool getScoreForAllLevels];
        int rescuedMakkus = 0;
        int deadMakkus = 0;
        int totalSeconds = 0;
        int totalScore = 0;

        for (NSDictionary *level in levelScores) {
            totalSeconds += [[level objectForKey:@"timeInSeconds"] intValue];
            deadMakkus += [[level objectForKey:@"deadMakkusCount"] intValue];
            rescuedMakkus += [[level objectForKey:@"rescuedMakkusCount"] intValue];
            totalScore += [[level objectForKey:@"score"] intValue];
        }
        
        int minutes = floor(totalSeconds / 60.0);
        int seconds = totalSeconds - (minutes * 60);
        NSString *minutesString = (minutes == 0) ? @"0" : [NSString stringWithFormat:@"%d",seconds];
        NSString *secondsString = (seconds < 10) ? [NSString stringWithFormat:@"0%d",seconds] : [NSString stringWithFormat:@"%d",seconds];
        
        SPQuad *overlay = [[SPQuad alloc] initWithWidth:screenWidth height:screenHeight color:0x000000];
        overlay.alpha = 0.8;

        SPTextField *rescuedDiedMakkusText = [[SPTextField alloc] initWithText:[NSString stringWithFormat:@"You've rescued %d Makkus in %d:%d minutes, but let die %d Makkus!", rescuedMakkus, minutesString, secondsString, deadMakkus]];
        rescuedDiedMakkusText.x = TEXTS_LEFT_POS_X;
        rescuedDiedMakkusText.y = 100;
        rescuedDiedMakkusText.height = 150;
        rescuedDiedMakkusText.fontName = @"Chalkduster";
        rescuedDiedMakkusText.fontSize = 16;
        rescuedDiedMakkusText.hAlign = SPHAlignLeft;
        
        SPImage *backgroundImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"levelStory_background"]];
        backgroundImage.x = (screenWidth - backgroundImage.width) / 2;
        backgroundImage.y = 50;
        
        SPImage *pageImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"GameCompleteScreen"]];
        pageImage.x = 0;
        pageImage.y = 15;
        
        // BUTTONS
        menuButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"menuButton"] downState:[playButtonsAtlas textureByName:@"menuButton_down"]];
        menuButton.x = (screenWidth / 2) - (menuButton.width / 2) - BUTTON_OFFSET;
        menuButton.y = 260;

        // RELEASES
        [playButtonsAtlas release];
        [playItemsAtlas release];
    }
    return self;
}

- (void)dealloc
{    
    [super dealloc];
}


#pragma mark - Trigger Events

-(void)onMenuButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowMenu object:nil]; 
}

@end
