//
//  IntroView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "IntroView.h"
#import "GameNotifications.h"

#define TEXT_FULL_WIDTH 380
#define TEXT_STORY_WIDTH 265
#define TEXT_STORY_POS_Y 115
#define TEXT_NORMAL_POS_Y 95
#define BUTTON_OFFSET 50

@interface IntroView()
-(void)onNextButtonTriggered:(SPEvent *)event;
-(void)onSkipButtonTriggered:(SPEvent *)event;
-(void)onStartButtonTriggered:(SPEvent *)event;
-(void)resetView;
-(void)renderView;
@end

@implementation IntroView

@synthesize nextButton;
@synthesize startButton;
@synthesize skipButton;
@synthesize levelImage;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    [storyTextes release];
    [levelMainTextes release];
    [levelQuestTextes release];
    
    [chamberAtlas release];
    [overlay release];
    [backgroundImage release];
    [storyImage release];
    [levelImage release];
    [mainText release];
    [questText release];
    [nextButton release];
    [skipButton release];
    [startButton release];
    [super dealloc];
}

- (void)loadForLevel:(int)theLevel withStory:(BOOL)theShowStory {    
    level = theLevel;
    showStory = theShowStory;
    
    [self resetView];
    [self renderView];          
}


#pragma mark - Private Methods

- (void)resetView {
    curPageIndex = 0;
    mainText.text = @"";
    questText.text = @"";
    nextButton.x = skipButton.x + BUTTON_OFFSET * 2;
    
}

- (void)renderView {
    if (level == 1 && showStory && curPageIndex < [storyTextes count]) {
        if (curPageIndex == 0) {       
            [self addChild:storyImage];
            [self addChild:skipButton];
            [self addChild:nextButton];
            [skipButton addEventListener:@selector(onSkipButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
            [nextButton addEventListener:@selector(onNextButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        } else if (curPageIndex == [storyTextes count] - 1) {
            [self removeChild:skipButton];
            nextButton.x = startButton.x;
        }
        
        mainText.width = TEXT_STORY_WIDTH;
        mainText.y = TEXT_STORY_POS_Y;
        mainText.text = [storyTextes objectAtIndex:curPageIndex];
        curPageIndex++;
        
    } else {
        if (curPageIndex > 0) {
            [self removeChild:storyImage];
            [self removeChild:nextButton];
            [skipButton removeEventListener:@selector(onSkipButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
            [nextButton removeEventListener:@selector(onNextButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        }

        self.levelImage = [SPImage imageWithTexture:[chamberAtlas textureByName:[NSString stringWithFormat:@"level%d_text", level]]];
        self.levelImage.x = 0;
        self.levelImage.y = 15.0;
        
        mainText.width = TEXT_FULL_WIDTH;
        mainText.y = TEXT_NORMAL_POS_Y;
        mainText.text = [levelMainTextes objectAtIndex:level-1];
        questText.text = [levelQuestTextes objectAtIndex:level-1];
        
        [self addChild:levelImage];
        [self addChild:startButton];
        [startButton addEventListener:@selector(onStartButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    }    
}


#pragma mark - Trigger Events

-(void)onNextButtonTriggered:(SPEvent *)event {
    [self renderView];
}

-(void)onSkipButtonTriggered:(SPEvent *)event {
    curPageIndex = [storyTextes count];
    [self removeChild:skipButton];
    nextButton.x = startButton.x;
    [self renderView];
}

-(void)onStartButtonTriggered:(SPEvent *)event {
    [self removeChild:levelImage];
    [self removeChild:startButton];
    [startButton removeEventListener:@selector(onStartButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyStartLevel object:nil];
}

#pragma mark - View Management

- (void)viewWillShow {
    if (backgroundImage == nil) {
        chamberAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"chamber_titles.xml"];
        SPTextureAtlas *playItemsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_items.xml"];
        SPTextureAtlas *playButtonsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_buttons.xml"];
        
        showStory = YES;
        float screenWidth = [UIScreen mainScreen].bounds.size.height;
        float screenHeight = [UIScreen mainScreen].bounds.size.width;
        
        overlay = [[SPQuad alloc] initWithWidth:screenWidth height:screenHeight color:0x000000];
        overlay.alpha = 0.8;
        
        // IMAGES
        backgroundImage = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"levelStory_background"]];
        backgroundImage.x = (screenWidth - backgroundImage.width) / 2;
        backgroundImage.y = 50;
        
        storyImage = [[SPImage alloc] initWithTexture:[chamberAtlas textureByName:@"intro_part1"]];
        storyImage.x = 0;
        storyImage.y = 15;
        
        
        // TEXTS
        NSString *story1 = @"According to the legend, small creatures live in our houses, in the dark, collecting dust and having dancing parties.";
        NSString *story2 = @"The Makkurokuroske (Makkus for friends) are creatures of the dark and gather dust to survive.";
        NSString *story3 = @"They move around smoothly like carried by a small breeze, but watch out! they are extremely vulnerable to light.";
        NSString *story4 = @"Collect dust, avoid light and fulfill the quest!";
        storyTextes = [[NSMutableArray alloc] initWithObjects:story1,story2,story3,story4, nil];
        
        NSString *levelMain1 = @"Meet the Makkus and guide them through the chamber to the door, but be aware of dangerous light.";
        NSString *levelMain2 = @"";
        NSString *levelMain3 = @"";
        NSString *levelMain4 = @"";
        levelMainTextes = [[NSMutableArray alloc] initWithObjects:levelMain1,levelMain2, levelMain3, levelMain4, nil];
        
        NSString *levelQuest1 = @"Gather all dust";
        NSString *levelQuest2 = @"";
        NSString *levelQuest3 = @"";
        NSString *levelQuest4 = @"";
        levelQuestTextes = [[NSMutableArray alloc] initWithObjects:levelQuest1,levelQuest2, levelQuest3, levelQuest4, nil];
        
        mainText = [[SPTextField alloc] initWithText:@""];
        mainText.x = backgroundImage.x + 25;
        mainText.height = 150;
        mainText.fontName = @"Chalkduster";
        mainText.fontSize = 16;
        mainText.vAlign = SPVAlignTop;
        
        questText = [[SPTextField alloc] initWithText:@""];
        questText.x = mainText.x;
        questText.y = 200;
        questText.width = TEXT_FULL_WIDTH;
        questText.height = 50;
        questText.fontName = @"Chalkduster";
        questText.fontSize = 18;
        questText.vAlign = SPVAlignTop;
        
        
        // BUTTONS
        skipButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"skipButton"] downState:[playButtonsAtlas textureByName:@"skipButton_down"]];
        skipButton.x = (screenWidth / 2) - (skipButton.width / 2) - BUTTON_OFFSET;
        skipButton.y = 260;
        
        nextButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"nextButton"] downState:[playButtonsAtlas textureByName:@"nextButton_down"]];        
        nextButton.x = (screenWidth / 2) - (nextButton.width / 2) + BUTTON_OFFSET;
        nextButton.y = 260;
        
        startButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"startButton"] downState:[playButtonsAtlas textureByName:@"startButton_down"]];
        startButton.x = (screenWidth / 2) - (startButton.width / 2);
        startButton.y = 260;
        
        [self addChild:overlay];
        [self addChild:backgroundImage];
        [self addChild:mainText];
        [self addChild:questText];
        
        [playItemsAtlas release];
        [playButtonsAtlas release];

    }
}

- (void)viewDidHide {
    [self removeAllChildren];
    
    [storyTextes release];
    storyTextes = nil;
    [levelMainTextes release];
    levelMainTextes = nil;
    [levelQuestTextes release];
    levelQuestTextes = nil;
    
    [chamberAtlas release];
    chamberAtlas = nil;
    [overlay release];
    overlay = nil;
    [backgroundImage release];
    backgroundImage = nil;
    [storyImage release];
    storyImage = nil;
    [levelImage release];
    levelImage = nil;
    [mainText release];
    mainText = nil;
    [questText release];
    questText = nil;
    [nextButton release];
    nextButton = nil;
    [skipButton release];
    skipButton = nil;
    [startButton release];
    startButton = nil;

}

@end
