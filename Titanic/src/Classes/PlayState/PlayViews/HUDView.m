//
//  HUDView.m
//  Titanic
//
//  Created by Philipp Anger on 11.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "HUDView.h"
#import "GameNotifications.h"
#import "ObjectiveChipmunk.h"
#import "Constants.h"

@interface HUDView()
-(void)onPauseButtonTriggered:(SPEvent *)event;
@end

@implementation HUDView

- (id)init {
    self = [super init];
    if (self) {
        screenWidth = [UIScreen mainScreen].bounds.size.height;
        screenHeight = [UIScreen mainScreen].bounds.size.width;
        
        SPTextureAtlas *playItemsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_items.xml"];
        SPTextureAtlas *playButtonsAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"play_buttons.xml"];
        
        pauseButton = [[SPButton alloc] initWithUpState:[playButtonsAtlas textureByName:@"pausebutton"] downState:[playButtonsAtlas textureByName:@"pausebutton_down"]];
        
        //DUST BAR
        SPImage *dustBarOverlay = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"healthbar"]];
        
        dustBarLimit = [[SPImage alloc] initWithTexture:[playItemsAtlas textureByName:@"healthbar_goalreachedmarker"]];
        dustBarQuad = [[SPQuad alloc] initWithWidth:DUST_BAR_WIDTH height:DUST_BAR_HEIGHT color:DUST_BAR_COLOR];
        
        dustBarOverlay.x = DUST_BAR_POS_X;
        dustBarOverlay.y = DUST_BAR_POS_Y;
        dustBarQuad.x = DUST_BAR_POS_X + 2.0;
        dustBarQuad.y = DUST_BAR_POS_Y + 10.0;
        dustBarQuad.width = DUST_BAR_WIDTH;
        dustBarLimit.x = dustBarQuad.x + dustBarQuad.width - (dustBarQuad.width * 0.25);
        dustBarLimit.y = dustBarQuad.y - 17.0;
       	
        arrow = [[SPImage alloc] initWithTexture:[playButtonsAtlas textureByName:@"directionArrow_yellow"]];
        arrow.pivotX = arrow.width / 2;
        arrow.pivotY = arrow.height / 2;
        arrowDistanceRadius = fminf(screenWidth, screenHeight) * 0.5f - arrow.height;
        
        [self addChild:pauseButton];
        [self addChild:dustBarQuad];
        [self addChild:dustBarLimit];
        [self addChild:dustBarOverlay];
        
        // EVENT LISTENERS
        [pauseButton addEventListener:@selector(onPauseButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        
        // RELEASES
        [dustBarOverlay release];
        [playItemsAtlas release];
        [playButtonsAtlas release];
    }
    return self;
}

- (void)dealloc
{
    [pauseButton release];
    [dustBarQuad release];
    [dustBarLimit release];
    [arrow release];
    [super dealloc];
}


#pragma mark - Private Methods

- (void)updateDustBarForMaximum:(float)maximum andValue:(float)value andLimit:(float)limit {
    int widthPercent = (int) value / maximum * DUST_BAR_WIDTH;
    int widthLimit = (int) limit / maximum *DUST_BAR_WIDTH;
    
    if (widthPercent <= DUST_BAR_WIDTH) {
        dustBarQuad.width = widthPercent;
    } else {
        dustBarQuad.width = DUST_BAR_WIDTH;
    }
    
    if (value >= maximum) {
        dustBarQuad.color = DUST_BAR_COLOR_GOAL;
    } else {
        dustBarQuad.color = DUST_BAR_COLOR;
    }
    dustBarLimit.x = dustBarQuad.x + widthLimit - (dustBarLimit.width / 2.);
}

- (void)showArrowWithVector:(CGPoint) scaledNormal {
    if (![self containsChild:arrow])
        [self addChild:arrow];
    
    cpVect normal = cpvnormalize(scaledNormal);
    
    float angle = atan2f(normal.y, normal.x);
    arrow.rotation = angle + PI_HALF;
    
    cpVect scaledVec = cpvmult(normal, arrowDistanceRadius);
//    arrow.x = screenWidth / 2 + scaledVec.x;
//    arrow.y = screenHeight / 2 + scaledVec.y;
    arrow.x = screenWidth / 2;
    arrow.y = screenHeight / 2;

}

- (void)hideArrow {
    [self removeChild:arrow];
}

#pragma mark - Trigger Events

- (void)onPauseButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyPauseLevel object:nil];
}

@end
