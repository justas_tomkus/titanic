//
//  GameFinishedView.h
//  Titanic
//
//  Created by Philipp Anger on 24.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"

@interface GameFinishedView : SPSprite

@property (nonatomic, strong) SPButton *menuButton;

@end
