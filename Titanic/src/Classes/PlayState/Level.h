//
//  Level.h
//  Titanic
//
//  Created by Philipp Anger on 02.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"
#import "ObjectiveChipmunk.h"
#import "Makku.h"
#import "Goal.h"

@class TMXMap;

@interface Level : SPSprite {
    TMXMap *tmxMap;
    
    float screenWidth;
    float screenHeight;
    float touchRadius;
    
    NSMutableArray *makkus;
    Goal *goal;
    CGPoint goalCenter;
    CGPoint makkuSpawnPoint;
    CGPoint deltaMove;
    CGPoint centerOfGravity;
    
    SPQuad *gravityVisualisation;
    
    // SOUNDS
    SPSoundChannel *effectsSoundChannel;
    float effectVolume;
    SPSound *completedSound;
    SPSound *gameOverSound; 
}

@property (nonatomic,assign) int number;
@property (nonatomic,assign) int score;
@property (nonatomic,assign) int dustLimit;
@property (nonatomic,assign) float dustLimitPercentage;
@property (nonatomic,assign) float dustSaved;
@property (nonatomic,assign) int dustMaximum;
@property (nonatomic,assign) int collectedDust;
@property (nonatomic,assign) int playTimeAverage;
@property (nonatomic,assign) int playTimeInSeconds;
@property (nonatomic,assign) int deadMakkusCount;
@property (nonatomic,assign) int rescuedMakkusCount;
@property (nonatomic,assign) float levelWidth;
@property (nonatomic,assign) float levelHeight;
@property (nonatomic,retain) NSDate *startTime;
@property (nonatomic,retain) NSDate *stopTime;
@property (nonatomic,assign) CGPoint centerOfGravity;

- (id)initWithTMXMap:(TMXMap *)tmxMap;
- (void)startLevel;
- (void)pauseLevel;
- (void)resumeLevel;
- (void)retryLevel;
- (void)completedLevel;
- (void)failedLevel;
- (void)cloneMakku:(Makku *)makku;
- (void)makkuDied:(Makku *)makku;
- (void)update;
- (float)levelHealth;
- (void)setEffectsVolume:(float)value;

@end
