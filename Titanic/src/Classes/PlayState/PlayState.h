//
//  GameState.h
//  Titanic
//
//  Created by Philipp Anger on 02.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameState.h"
#import "ViewManagement.h"

@class Level, HUDView, PauseView, IntroView, LostView, CompletedView;

@interface PlayState : GameState {
    PauseView *pauseView;
    IntroView *introView;
    LostView *lostView;
    CompletedView *completedView;
    float screenWidth, screenHeight;
    SPSoundChannel *soundChannel;
    
    //Volume
    float effectVolume, musicVolume;
}

@property (nonatomic, strong) Level *currentLevel;
@property (nonatomic, strong) SPSprite<ViewManagement> *currentOverlay;
@property (nonatomic, strong) PauseView *pauseView;
@property (nonatomic, strong) IntroView *introView;
@property (nonatomic, strong) CompletedView *completedView;
@property (nonatomic, strong) HUDView *hudView;

- (id)initWithWidth:(float)theWidth andHeight:(float)theHeight;
- (void)loadLevel:(int)level;

@end
