//
//  Level.m
//  Titanic
//
//  Created by Philipp Anger on 02.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Level.h"
#import "Dust.h"
#import "ModuleManager.h"
#import "Couch.h"
#import "Goal.h"
#import "Light.h"
#import "TMXMap.h"
#import "TMXObject.h"
#import "ColliderInfo.h"
#import "Circle.h"
#import "Rectangle.h"
#import "RoundRectangle.h"
#import "GameNotifications.h"
#import "NSNotification+UserObject.h"
#import "Constants.h"

#define VIEWPORT_UPDATE_PERCENTAGE_FROM_SCREEN_BORDER 0.5
#define LIGHT_MAP_TILE_SIZE 512
#define DUST_LIMIT_PERCENTAGE 0.75f

@interface Level ()
- (void)setupMap;
- (void)addColliders;
- (void)addDusts;
- (void)addLights;
- (void)addGoalAndSpawnPoint;
- (void)updateCenterOfGravityForMakkus;
- (void)updateViewPort;
- (void)registerNotifications;
- (void)updateDust:(NSNotification *)notification;
@end

@implementation Level

@synthesize number;
@synthesize score;
@synthesize dustLimit;
@synthesize dustLimitPercentage;
@synthesize dustSaved;
@synthesize dustMaximum;
@synthesize collectedDust;
@synthesize playTimeAverage;
@synthesize playTimeInSeconds;
@synthesize deadMakkusCount;
@synthesize rescuedMakkusCount;
@synthesize levelWidth;
@synthesize levelHeight;
@synthesize startTime;
@synthesize stopTime;
@synthesize centerOfGravity;

- (id)initWithTMXMap:(TMXMap *)theTMXMap {
    self = [super init];
    if (self) {
        tmxMap = [theTMXMap retain];        
        
        self.levelWidth = tmxMap.width * tmxMap.tileWidth;
        self.levelHeight = tmxMap.height * tmxMap.tileHeight;
        self.width = levelWidth;
        self.height = levelHeight;
        self.x = tmxMap.x;
        self.y = tmxMap.y;
    
        makkus = [[NSMutableArray array] retain];
        screenWidth = [[UIScreen mainScreen] bounds].size.height;   // landscape orientation
        screenHeight = [[UIScreen mainScreen] bounds].size.width;   // landscape orientation
        makkuSpawnPoint = CGPointMake(screenWidth / 2, screenHeight / 2);
        
        deltaMove = CGPointMake(0, 0);
        touchRadius = 50;        
        
        // SOUNDS        
        completedSound = [[SPSound soundWithContentsOfFile:@"levelCompleted.aifc"] retain];
        gameOverSound = [[SPSound soundWithContentsOfFile:@"sadTrombone_1.aifc"] retain];
        
        
        [self setupMap];
        [self registerNotifications];
    }
    return self;
}

- (void)dealloc
{
    [tmxMap release];
    [makkus release];
    [gravityVisualisation release];
    
    [goal release];
    [startTime release];
    [stopTime release];
    
    // SOUNDS
    [effectsSoundChannel stop];
    [effectsSoundChannel release];
    [completedSound release];
    [gameOverSound release];
    [super dealloc];
}

- (void)setEffectsVolume:(float)value {
    effectVolume = value;
    [effectsSoundChannel setVolume:value];
}

-(void)playSound:(SPSound*)sound andLoop:(BOOL) doLoop {
    [effectsSoundChannel release];
    effectsSoundChannel = [[sound createChannel] retain];
    [effectsSoundChannel setVolume:effectVolume];
    effectsSoundChannel.loop = doLoop;        
    [effectsSoundChannel play];
}

- (void)cloneMakku:(Makku *)makku {
    Makku *newMakku = [[Makku alloc] initWithPosition:makku.body.pos];
    [newMakku setEffectsVolume:effectVolume];
    [makkus addObject:newMakku];
    [self addChild:newMakku];
    [newMakku.body applyImpulse:makku.body.vel offset:cpv(0, 0)];
    [newMakku release];
    self.rescuedMakkusCount++;
    
    [makku wasCloned];
}

- (void)makkuDied:(Makku *)makku {
    [makkus removeObject:makku];
    [makku kill];
    self.deadMakkusCount++;
    self.rescuedMakkusCount--;
    if ([makkus count] <= 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyGameOver object:nil];
    }
}

- (void)startLevel {
    self.startTime = [NSDate date];
    self.deadMakkusCount = 0;
    self.collectedDust = [self levelHealth];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyDustBarUpdate object:nil];
}

- (void)pauseLevel {
    self.stopTime = [NSDate date];
    NSTimeInterval diff = [self.stopTime timeIntervalSinceDate:self.startTime];
    self.playTimeInSeconds += (int) diff % 60;
}

- (void)resumeLevel {
    self.startTime = [NSDate date];
}

- (void)retryLevel {
    
    deltaMove = CGPointMake(0, 0);
    
    [makkus release];
    makkus = [[NSMutableArray array] retain];
    
    [self removeAllChildren];
    
    [self setupMap];
}

- (void)completedLevel {
    self.stopTime = [NSDate date];
    NSTimeInterval diff = [self.stopTime timeIntervalSinceDate:self.startTime];
    self.playTimeInSeconds += (int) diff % 60;
    
 
    
    [self playSound:completedSound andLoop:NO];
    
}

- (void)failedLevel {
    [self playSound:gameOverSound andLoop:NO];
}


#pragma mark - Private Methods

- (void)setupMap {
    [self removeAllChildren];
    [[ModuleManager sharedModuleManager].physicsModule resetPhysics];
    [[ModuleManager sharedModuleManager].physicsModule defineSpaceBounds:CGRectMake(self.x, self.y, self.levelWidth, self.levelHeight) andFriction:1.0f];
    
    self.dustLimitPercentage = [[tmxMap.properties objectForKey:@"MinimumDust"] floatValue];
    self.playTimeAverage = [[tmxMap.properties objectForKey:@"PlayTimeAverage"] intValue];
    int nrOfMakkus = [[tmxMap.properties objectForKey:@"NumberOfMakkus"] intValue];
    
    self.playTimeInSeconds = 0;
    self.collectedDust = 0;
    self.dustSaved = 0;
    self.dustMaximum = 0;
    self.dustLimit = 0;
    self.rescuedMakkusCount = 1;
    self.deadMakkusCount = 0;
    
    [self addChild:tmxMap];
    [self addColliders];
    [self addLights];
    [self addDusts];
    [self addGoalAndSpawnPoint];
    
    
    // MAKKU
    for (int i=0; i<nrOfMakkus; ++i) {
        Makku *makku = [[Makku alloc] initWithPosition:makkuSpawnPoint];
	[makku setEffectsVolume:effectVolume];
        [makkus addObject:makku];
        [self addChild:makku];
        [makku release];
    }
    
    dustMaximum += [self levelHealth];
    dustLimit = dustMaximum * dustLimitPercentage;
    
    self.x = - (makkuSpawnPoint.x - (screenWidth / 2.));
    self.y = - (makkuSpawnPoint.y - (screenHeight / 2.));

    // SAFETY UPDATE
    [self update];
    [self addEventListener:@selector(onTouch:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
}

- (void)onTouch:(SPTouchEvent*)event
{
    SPTouch *touch = [[event touchesWithTarget:self andPhase:SPTouchPhaseBegan] anyObject];
    if (touch)
    {
        SPPoint *touchPosition = [touch locationInSpace:self];
        
//        cpFloat distance = cpvlength(cpv(touchPosition.x - centerOfGravity.x, touchPosition.y - centerOfGravity.y));
//        if (distance > touchRadius)
//            return;
        
        for (Makku *m in makkus) {
            cpVect diff = cpvsub(cpv(touchPosition.x, touchPosition.y), m.body.pos);
            m.body.vel = cpvzero;
            [m.body resetForces];
//            [m.body applyForce:diff offset:cpvzero];
            [m.body applyImpulse:diff offset:cpvzero];
//            m.body.force = diff;
            //        m.body.pos = cpv(makkuSpawnPoint.x, makkuSpawnPoint.y);
            //        m.body.vel = cpvzero;
            //        [m.body resetForces];
        }
    }
}

- (void)addColliders {
    
    TMXObjectGroup *objGrp = [tmxMap.objectGroups objectForKey:@"Colliders"];
    
    for (NSString *key in [objGrp.objects allKeys]) {
        TMXObject *obj = [objGrp.objects valueForKey:key];
        ColliderInfo *colInfo = [[[ColliderInfo alloc] init] autorelease];
        colInfo.x = obj.x;
        colInfo.y = obj.y;
        colInfo.width = obj.width;
        colInfo.height = obj.height;
        
        colInfo.elasticity = [[obj.properties objectForKey:@"elasticity"] floatValue];
        colInfo.friction = [[obj.properties objectForKey:@"friction"] floatValue];
        
        NSString *type = [obj.properties objectForKey:@"type"];
        
        if ([type isEqualToString:@"Circle"]) {
            Collider * col = [[[Circle alloc] initWithCInfo:colInfo] autorelease];
            [self addChild:col];
            continue;
        }
        if ([type isEqualToString:@"Rectangle"]){
            Collider * col = [[[Rectangle alloc] initWithCInfo:colInfo] autorelease];
            [self addChild:col];
            continue;
        }
        if ([type isEqualToString:@"RoundedRectangle"]){
            Collider * col = [[Rectangle alloc] initWithCInfo:colInfo];
            [self addChild:col];
            [col release];
            continue;
        }
    }
}

- (void)addLights {
    
    TMXObjectGroup *objGrp = [tmxMap.objectGroups objectForKey:@"Light sources"];
    
    //adding light colliders
    for (NSString *key in [objGrp.objects allKeys]) {
        TMXObject *obj = [objGrp.objects valueForKey:key];
        float intensity = [[obj.properties objectForKey:@"intensity"] floatValue];
        
        if ([obj.objType isEqualToString:@"Light"]) {
            //creating
            ColliderInfo *colInfo = [[[ColliderInfo alloc] init] autorelease];
            colInfo.x = obj.x;
            colInfo.y = obj.y;
            colInfo.width = obj.width;
            colInfo.height = obj.height;
            
            Light * light = [[[Light alloc] initWithCInfo:colInfo] autorelease];
            light.intensity = intensity;
            [self addChild:light];
        }                
    }
    
    int widthImageCount = (int)ceil(tmxMap.displayWidth / (float)(LIGHT_MAP_TILE_SIZE));
    int heightImageCount = (int)ceil(tmxMap.displayHeight / (float)(LIGHT_MAP_TILE_SIZE));
    
    for (int h=0; h<heightImageCount; ++h) {
        for (int w=0; w<widthImageCount; ++w) {
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(LIGHT_MAP_TILE_SIZE, LIGHT_MAP_TILE_SIZE), NO, 1.0);
            
            int offsetX = LIGHT_MAP_TILE_SIZE * w;
            int offsetY = LIGHT_MAP_TILE_SIZE * h;
            
            CGContextRef context = UIGraphicsGetCurrentContext();
            
            CGContextSetFillColorWithColor(context, [UIColor colorWithWhite:0.2 alpha:0.7].CGColor);
            CGContextSetBlendMode(context, kCGBlendModeMultiply);
            CGContextSetAlpha(context, 1);
            
            CGContextFillRect(context, CGRectMake(CGPointZero.x, CGPointZero.y, LIGHT_MAP_TILE_SIZE, LIGHT_MAP_TILE_SIZE));
            
            CGContextSetBlendMode(context, kCGBlendModeClear);
            
            for (NSString *key in [objGrp.objects allKeys]) {
                TMXObject *obj = [objGrp.objects valueForKey:key];
                int radius = obj.width/2.;
                CGContextSetFillColorWithColor(context, [UIColor colorWithWhite:1 alpha:1].CGColor);
                
                CGContextFillEllipseInRect(context, CGRectMake((obj.x - radius + obj.width/2) - offsetX, (obj.y - radius + obj.height/2) - offsetY, obj.width, obj.width));
            } 
            
            UIImage * light = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            SPImage *lightMap = [SPImage imageWithTexture:[SPTexture textureWithWidth:light.size.width height:light.size.height draw:^(CGContextRef context) {
                [light drawAtPoint:CGPointMake(CGPointZero.x, CGPointZero.y)];
            }]];
            
            lightMap.x = offsetX;
            lightMap.y = offsetY;
            
            [self addChild:lightMap];
        }
    }    
}

- (void)addDusts {
    TMXObjectGroup *objGrp = [tmxMap.objectGroups objectForKey:@"Dust zones"];
    dustMaximum = 0;
    
    for (NSString *key in [objGrp.objects allKeys]) {
        TMXObject *obj = [objGrp.objects valueForKey:key];
        
        int x = obj.x + (obj.width/2.) - (DUST_SIZE/2.);
        int y = obj.y + (obj.height/2.) - (DUST_SIZE/2.);
        
        Dust *dust = [[Dust alloc] initWithPosition:CGPointMake(x, y) andDirtiness:[[obj.properties objectForKey:@"dirtiness"] floatValue]];
        
        
        
        
        dustMaximum += dust.dirtiness;
        
        [self addChild:dust];
        [dust release];
    }
}

- (void)addGoalAndSpawnPoint {
    TMXObjectGroup *objGrp = [tmxMap.objectGroups objectForKey:@"SpammingAndGoal"];
    
    for (NSString *key in [objGrp.objects allKeys]) {
        TMXObject *obj = [objGrp.objects valueForKey:key];
        
        if ([key isEqualToString:@"SpawnigPoint"]) {
            makkuSpawnPoint = CGPointMake(obj.x, obj.y);
        }else if ([key isEqualToString:@"GoalZone"]) {
            ColliderInfo *colInfo = [[ColliderInfo alloc] init];
            colInfo.x = obj.x;
            colInfo.y = obj.y;
            colInfo.width = obj.width;
            colInfo.height = obj.height;
            
            goal = [[Goal alloc] initWithCInfo:colInfo];
            int x = goal.position.x + goal.width / 2;
            int y = goal.position.y + goal.height / 2;
            goalCenter = CGPointMake(x, y);
            [self addChild:goal];
	    [colInfo release];
        }
    }

}

- (BOOL) isDustLimitReached {
    if (collectedDust >= dustLimit) {
        return YES;
    }
    return NO;
}

- (void) registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDust:) name:kNotifyDustCollected object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDust:) name:kNotifyLightDamage object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goalCollisionWithMakku:) name:kNotifyGoalCollision object:nil];
}


#pragma mark - Notifications Methods

- (void)updateDust:(NSNotification *)notification {
    collectedDust = [self levelHealth];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyDustBarUpdate object:nil];
}

- (void) goalCollisionWithMakku:(NSNotification *)notification {
    Makku* makku = [notification.userInfo objectForKey:@"makku"];
    Goal *goal = [notification.userInfo objectForKey:@"goal"];

    if ([self isDustLimitReached]) {
        //play sound
        [makku goalReached];
        dustSaved += makku.makkuHealth;
        
        //remove silently
        [[ModuleManager sharedModuleManager].physicsModule removeDynamicBody:makku.body andShapes:makku.shapes];
        [makkus removeObject:makku];
        
        //move to goal center and fade out
        SPTween *animation = [SPTween tweenWithTarget:makku time:1.0 transition:SP_TRANSITION_EASE_OUT];
        //[animation animateProperty:@"rotation" targetValue:makku.rotation + SP_D2R(45)];
        [animation animateProperty:@"alpha" targetValue:0.0f];
        [animation moveToX:goalCenter.x y:goalCenter.y];
        
        SPJuggler * juggler = [SPStage mainStage].juggler;
        [juggler addObject:animation];
        
        if ([makkus count] <= 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyCompleteLevel object:nil];
        }
    }
}

#pragma mark - Update Methods

- (void)update {
    for (Makku *makku in makkus) {
        [makku update];
    }
    
    [self updateCenterOfGravityForMakkus];
    [self updateViewPort];
    [self updateArrow];
}

- (void)updateArrow {
    if ([self isDustLimitReached] == NO)
        return;
    
    if ([self isPointOutOfSight:goalCenter] == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyHideArrow object:nil];
        return;
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowArrowWithGoal object:goal];
}

- (BOOL)isPointOutOfSight:(CGPoint)point {
    if (-self.x > point.x)
        return YES;
    if (-self.x + screenWidth < point.x)
        return YES;
    if (-self.y > point.y)
        return YES;
    if (-self.y + screenHeight < point.y)
        return YES;
    return NO;
}

- (void)updateCenterOfGravityForMakkus {
    float avgX = 0.0;
    float avgY = 0.0;
    
    for (Makku *makku in makkus) {
        avgX += makku.x;
        avgY += makku.y;
    }
    
    CGPoint oldCenterOfGravity = CGPointMake(centerOfGravity.x, centerOfGravity.y);
    centerOfGravity = CGPointMake(avgX / (float)[makkus count], avgY / (float)[makkus count]);
    deltaMove.x = centerOfGravity.x - oldCenterOfGravity.x;
    deltaMove.y = centerOfGravity.y - oldCenterOfGravity.y;
    gravityVisualisation.x = centerOfGravity.x - gravityVisualisation.width / 2;
    gravityVisualisation.y = centerOfGravity.y - gravityVisualisation.height / 2;
}

- (void)updateViewPort {   
    float moveRangeX = (screenWidth * VIEWPORT_UPDATE_PERCENTAGE_FROM_SCREEN_BORDER) / 2.0;
    float moveRangeY = (screenHeight * VIEWPORT_UPDATE_PERCENTAGE_FROM_SCREEN_BORDER) / 2.0;
    float newLevelX = self.x + (deltaMove.x * -1.0);
    float newLevelY = self.y + (deltaMove.y * -1.0);

    // BORDER RANGE
    BOOL isInBorderRangeTop = (centerOfGravity.y <= moveRangeY) ? YES : NO;
    BOOL isInBorderRangeRight = (centerOfGravity.x >= (levelWidth - moveRangeX)) ? YES : NO;
    BOOL isInBorderRangeBottom = (centerOfGravity.y >= (levelHeight - moveRangeY)) ? YES : NO;
    BOOL isInBorderRangeLeft = (centerOfGravity.x <= moveRangeX) ? YES : NO;

    // MOVE RANGE
    BOOL isInMoveRangeTop = (centerOfGravity.y <= ((self.y * -1) + moveRangeY)) ? YES : NO;
    BOOL isInMoveRangeRight = (centerOfGravity.x >= (((self.x * -1) + screenWidth) - moveRangeX)) ? YES : NO;
    BOOL isInMoveRangeBottom = (centerOfGravity.y >= (((self.y * -1) + screenHeight) - moveRangeY)) ? YES : NO;
    BOOL isInMoveRangeLeft = (centerOfGravity.x <= ((self.x * -1) + moveRangeX)) ? YES : NO;

    
    if (isInBorderRangeLeft) {
        self.x = 0.0;
    } else if (isInMoveRangeLeft) {
        self.x = newLevelX;
    }
    
    if (isInBorderRangeRight) {
        self.x = -(levelWidth - screenWidth);
    } else if (isInMoveRangeRight) {
        self.x = newLevelX;
    }
    
    if (isInBorderRangeTop) {
        self.y = 0.0;
    } else if (isInMoveRangeTop) {
        self.y = newLevelY;
    }
    
    if (isInBorderRangeBottom) {
        self.y = -(levelHeight - screenHeight);
    } else if (isInMoveRangeBottom) {
        self.y = newLevelY;
    }
    
    // SAFETY CONDITIONS
    if (self.x < -(levelWidth - screenWidth)) {
        self.x = -(levelWidth - screenWidth);
    } else if (self.x > 0.0) {
        self.x = 0.0;
    }
    
    if (self.y < -(levelHeight - screenHeight)) {
        self.y = -(levelHeight - screenHeight);
    } else if (self.y > 0.0) {
        self.y = 0.0;
    }
}

- (float)levelHealth {
    float h = dustSaved;
    for (Makku* makku in makkus) {
        h += makku.makkuHealth;
    }
    return h;
}

@end
