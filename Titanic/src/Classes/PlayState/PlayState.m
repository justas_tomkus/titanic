//
//  GameState.m
//  Titanic
//
//  Created by Philipp Anger on 02.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "PlayState.h"
#import "Level.h"
#import "HUDView.h"
#import "PauseView.h"
#import "IntroView.h"
#import "LostView.h"
#import "CompletedView.h"
#import "GameNotifications.h"
#import "Makku.h"
#import "Goal.h"
#import "ModuleManager.h"
#import "OptionsMenuView.h"

@interface PlayState()
- (void)registerNotifications;
- (void)spEnterFrame:(SPEnterFrameEvent *)event;
- (void)makkuIsCloning:(NSNotification *)notification;
- (void)makkuDied:(NSNotification *)notification;
- (void)dustBarUpdate:(NSNotification *)notification;
- (void)levelStarted:(NSNotification *)notification;
- (void)levelPaused:(NSNotification *)notification;
- (void)levelResumed:(NSNotification *)notification;
- (void)levelRetry:(NSNotification *)notification;
- (void)levelCompleted:(NSNotification *)notification;
- (void)levelQuit:(NSNotification *)notification;
- (void)loadNextLevel:(NSNotification *)notification;
- (void)showOverlay:(SPSprite<ViewManagement> *)overlay;
- (void)hideOverlay:(SPSprite<ViewManagement> *)overlay;
- (void)hideAllOverlays;
@end

@implementation PlayState

@synthesize currentLevel;
@synthesize currentOverlay;
@synthesize pauseView;
@synthesize completedView;
@synthesize introView;
@synthesize hudView;

- (id)initWithWidth:(float)theWidth andHeight:(float)theHeight {
    self = [super initWithWidth:theWidth andHeight:theHeight];
    if (self) {        
        
        screenWidth = [UIScreen mainScreen].bounds.size.height;
        screenHeight = [UIScreen mainScreen].bounds.size.width;
        
        effectVolume = FX_VOLUME;
        musicVolume = MUSIC_VOLUME;
        [self registerNotifications];
    }
    return self;
}

- (void)dealloc
{
    [soundChannel stop];
    [soundChannel release];
    [pauseView release];
    [introView release];
    [lostView release];
    [completedView release];
    [currentLevel release];
    [currentOverlay release];
    [hudView release];
    [super dealloc];
}

- (PauseView *)pauseView {
    if (pauseView == nil) {
        pauseView = [[PauseView alloc] init];
    }
    return pauseView;
}

- (CompletedView *)completedView {
    if (completedView == nil) {
        completedView = [[CompletedView alloc] init];
    }
    return completedView;
}

- (HUDView *)hudView {
    if (hudView == nil) {
        hudView = [[HUDView alloc] init];
    }
    return hudView;
}

- (IntroView *)introView {
    if (introView == nil) {
        introView = [[IntroView alloc] init];
    }
    return introView;
}

- (LostView *)lostView {
    if (lostView == nil) {
        lostView = [[LostView alloc] init];
    }
    return lostView;
}

- (void)stateWillShow {
    if (!soundChannel) {
        SPSound *music = [[SPSound alloc] initWithContentsOfFile:@"playMusic.aifc"];
        soundChannel = [[music createChannel] retain];
        soundChannel.volume = musicVolume;
        soundChannel.loop = YES;
        [music release];
    }
}

- (void)stateDidHide {
    
}

- (void)loadLevel:(int)level {
    [self hideAllOverlays];
    [self removeAllChildren];
    
    [currentLevel release];
    currentLevel = [[Level alloc] initWithTMXMap:[[ModuleManager sharedModuleManager].levelModule loadTMXMapForLevel:level]];
    currentLevel.number = level;
    [currentLevel setEffectsVolume:effectVolume];
    
    
    [self addChild:self.currentLevel];
    [self addChild:self.hudView];
    [self showOverlay:self.introView];
    [self.introView loadForLevel:level withStory:YES];
    
    
    // TESTING
    
//    [completedView loadForLevel:1 andSuccess:YES];
//    [self addChild:currentLevel];
//    [self addChild:currentOverlay];
}

#pragma mark - Enter Frame - GAME LOOP

- (void)spEnterFrame:(SPEnterFrameEvent *)event {
    [[ModuleManager sharedModuleManager] update];
    [self.currentLevel update];
}


#pragma mark - Private Methods

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(makkuIsCloning:) name:kNotifyMakkuIsCloning object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(makkuDied:) name:kNotifyMakkuDied object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dustBarUpdate:) name:kNotifyDustBarUpdate object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(levelStarted:) name:kNotifyStartLevel object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(levelPaused:) name:kNotifyPauseLevel object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(levelResumed:) name:kNotifyResumeLevel object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(levelRetry:) name:kNotifyRetryLevel object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(levelCompleted:) name:kNotifyCompleteLevel object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(levelQuit:) name:kNotifyQuitLevel object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(levelFailed:) name:kNotifyGameOver object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadNextLevel:) name:kNotifyNextLevel object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showArrowWithGoal:) name:kNotifyShowArrowWithGoal object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideArrow:) name:kNotifyHideArrow object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setMusicVolume:) name:kNotifySetMusicVolume object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setEffectVolume:) name:kNotifySetEffectVolume object:nil];
}


#pragma mark - Notifications Methods

- (void)makkuIsCloning:(NSNotification *)notification {
    [self.currentLevel cloneMakku:notification.object];
}

- (void)makkuDied:(NSNotification *)notification {
    [self.currentLevel makkuDied:notification.object];
}

- (void)dustBarUpdate:(NSNotification *)notification {
    [self.hudView updateDustBarForMaximum:currentLevel.dustMaximum andValue:currentLevel.collectedDust andLimit:self.currentLevel.dustLimit];
}

- (void)levelStarted:(NSNotification *)notification {
    [self hideOverlay:currentOverlay];
    
    if (![soundChannel isPlaying]) {
        [soundChannel stop];
        [soundChannel play];
    }
    
    [self.currentLevel startLevel];
    [self addEventListener:@selector(spEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
}

- (void)levelPaused:(NSNotification *)notification {
    [self showOverlay:self.pauseView];
    
    if ([soundChannel isPlaying]) {
        [soundChannel pause];
    }
    
    [self.currentLevel pauseLevel];
    [self removeEventListener:@selector(spEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
}

- (void)levelResumed:(NSNotification *)notification {
    [self hideOverlay:currentOverlay];
    
    if ([soundChannel isPaused]) {
        [soundChannel play];
    }
    
    [self.currentLevel resumeLevel];
    [self addEventListener:@selector(spEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
}

- (void)levelRetry:(NSNotification *)notification {
    [self hideAllOverlays];
    [self removeAllChildren];
    [self removeEventListener:@selector(spEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
    
    if ([soundChannel isPlaying]) {
        [soundChannel stop];
    }
    
    [currentLevel retryLevel];
    
    
    [self addChild:self.currentLevel];
    [self addChild:self.hudView];
    [self showOverlay:self.introView];
    [self.introView loadForLevel:currentLevel.number withStory:NO];
}

- (void)levelCompleted:(NSNotification *)notification {
    [[ModuleManager sharedModuleManager].levelModule saveScoreForLevel:self.currentLevel];
    
    [self showOverlay:self.completedView];
    [self.completedView loadForLevel:self.currentLevel.number andSuccess:YES];
    
    if ([soundChannel isPlaying]) {
        [soundChannel stop];
    }
    
    [self.currentLevel completedLevel];
    [self removeEventListener:@selector(spEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
}

- (void)showArrowWithGoal:(NSNotification *) notification {
    Goal *goal = notification.object;
    cpVect goalPos = cpv(goal.x + goal.width/2, goal.y + goal.height/2);
    cpVect levelPos = cpv(-currentLevel.x, -currentLevel.y);

    cpVect screenCenter = cpv(screenWidth/2, screenHeight/2);
    screenCenter = cpvadd(screenCenter, levelPos);
    
    cpVect diff = cpvsub(goalPos, screenCenter);

    [self.hudView showArrowWithVector:diff];
}

-(void)hideArrow:(NSNotification*) notification {
    [self.hudView hideArrow];
}

- (void)levelQuit:(NSNotification *)notification {
    [self hideOverlay:currentOverlay];
    [self removeEventListener:@selector(spEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
    
    if ([soundChannel isPlaying]) {
        [soundChannel stop];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowMenu object:nil];
}

- (void)levelFailed:(NSNotification *)notification {
    [self showOverlay:self.lostView];
    
    if ([soundChannel isPlaying]) {
        [soundChannel stop];
    }
    
    [self.currentLevel failedLevel];
    [self removeEventListener:@selector(spEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
}

- (void)loadNextLevel:(NSNotification *)notification {
    int next = currentLevel.number+1;
    
    if (next <= 4) {
        [self loadLevel:next];
    }else {
        [self levelQuit:nil];
    }
}

- (void)setMusicVolume:(NSNotification*)notification {
    NSNumber *value = notification.object;
    musicVolume = value.floatValue;
    [soundChannel setVolume:musicVolume];
}

- (void)setEffectVolume:(NSNotification*)notification {
    NSNumber *value = notification.object;
    effectVolume = value.floatValue;
}

- (void)showOverlay:(SPSprite<ViewManagement> *)overlay {
    [self removeChild:self.currentOverlay];
    [self.currentOverlay viewDidHide];
    
    self.currentOverlay = overlay;
    
    [self.currentOverlay viewWillShow];
    [self addChild:self.currentOverlay];
}

- (void)hideOverlay:(SPSprite<ViewManagement> *)overlay {
    [self removeChild:overlay];
    [overlay viewDidHide];
}

- (void)hideAllOverlays {
    [self hideOverlay:self.pauseView];
    [self hideOverlay:self.introView];
    [self hideOverlay:self.completedView];
}


@end
