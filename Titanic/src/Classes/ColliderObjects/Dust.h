//
//  Dust.h
//  Titanic
//
//  Created by Philipp Anger on 03.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Collider.h"

@interface Dust : Collider {
    SPImage *image;
    BOOL collected;
    float dirtiness;
}

@property (nonatomic, assign) BOOL collected;
@property (nonatomic, assign) float dirtiness;

- (id)initWithPosition:(CGPoint)thePosition andDirtiness:(float)theDirtiness;
- (void)wasCollected;

@end
