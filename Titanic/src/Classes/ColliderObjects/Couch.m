//
//  Couch.m
//  Titanic
//
//  Created by Philipp Anger on 21.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Couch.h"
#import "ModuleManager.h"

#ifndef COUCH_WIDTH
    #define COUCH_WIDTH 100.0
#endif

#ifndef COUCH_HEIGHT
    #define COUCH_HEIGHT 50.0
#endif

#ifndef COUCH_ELASTICITY
    #define COUCH_ELASTICITY 0.3
#endif

#ifndef COUCH_FRICTION
    #define COUCH_FRICTION 1.0
#endif

@implementation Couch

- (id)initWithPosition:(CGPoint)thePosition {
    self = [super initWithPosition:thePosition width:COUCH_WIDTH height:COUCH_HEIGHT andShapeType:ShapeTypeRectangle];
    if (self) {
        
        // PHYSICS
        body = [[ChipmunkBody alloc] initStaticBody];
        self.body.pos = cpv(self.x, self.y);
        
        //Bounding Boxes
        cpVect sideBB[] = {
            cpv(0, 0),
            cpv(0, 50.0),
            cpv(8.0, 50.0),
            cpv(8.0, 0),
        };
        
        cpVect midBB[] = {
            cpv(0, 0),
            cpv(0, 35.0),
            cpv(84.0, 35.0),
            cpv(84.0, 0),
        };
        
        // positions of the bounding Boxes relative to body position
        cpVect leftSidePos = cpv(0,0), midPos = cpv(8.0, 0), rightSidePos = cpv(8.0 + 84.0, 0);
        
        //leftSide Shape
        ChipmunkShape *shapeLeft = [[ChipmunkStaticPolyShape alloc] initWithBody:self.body count:4 verts:sideBB offset:leftSidePos];
                     
        shapeLeft.collisionType = (id)TypeSofa;
        shapeLeft.elasticity = COUCH_ELASTICITY;
        shapeLeft.friction = COUCH_FRICTION;
        
        [self.shapes addObject:shapeLeft];
        [shapeLeft release];
        
        //mid Shape
        
        ChipmunkShape *shapeMid = [[ChipmunkStaticPolyShape alloc] initWithBody:self.body count:4 verts:midBB offset:midPos];
        
        shapeMid.collisionType = (id)TypeSofa;
        shapeMid.elasticity = COUCH_ELASTICITY;
        shapeMid.friction = COUCH_FRICTION;
        
        [self.shapes addObject:shapeMid];
        [shapeMid release];
        
        //rightSide Shape
        
        ChipmunkShape *shapeRight = [[ChipmunkStaticPolyShape alloc] initWithBody:self.body count:4 verts:sideBB offset:rightSidePos];
        
        shapeRight.collisionType = (id)TypeSofa;
        shapeRight.elasticity = COUCH_ELASTICITY;
        shapeRight.friction = COUCH_FRICTION;
        
        [self.shapes addObject:shapeRight];
        [shapeRight release];

        // MODULES
        //adding to physics space
        [[ModuleManager sharedModuleManager].physicsModule addStaticBodyWithShapes:self.shapes];
        
        // IMAGE STUFF
        image = [[SPImage alloc] initWithContentsOfFile:@"couch.png"];
        image.width = COUCH_WIDTH;
        image.height = COUCH_HEIGHT;
        [self addChild:image];
        [image release];
        // COLLIDER-OBJECT STUFF
        [self drawBoundingWithOffset:leftSidePos andRightBottomOffset:cpv(8.0,50.0)];
        [self drawBoundingWithOffset:midPos andRightBottomOffset:cpv(84.0,35.0)];
        [self drawBoundingWithOffset:rightSidePos andRightBottomOffset:cpv(8.0,50.0)];
    }
    return self;
}

- (void)dealloc
{
    [image release];
    [super dealloc];
}


@end
