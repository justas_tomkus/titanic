//
//  Makku.h
//  Titanic
//
//  Created by Philipp Anger on 05.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
#import "Collider.h"

@class Dust;

@interface Makku : Collider {
    float makkuHealth;
    
    // SOUNDS
    SPSoundChannel *soundChannel;
    float effectVolume;
    NSMutableSet *touchedSounds;
    NSMutableSet *hurtSounds;
    NSMutableSet *diedSounds;
    NSMutableSet *clonedSounds;
    NSMutableSet *gatheredDustSounds;
    NSMutableSet *goalReachedSounds;
    NSMutableSet *sneezeSounds;
    NSMutableSet *gigglingSounds;
    NSMutableSet *movesSounds;
    
    // ANIMATIONS
    SPMovieClip *currentMovieClip;
    NSTimer *animationTimer;
    
    SPMovieClip *closeEyesClip;

    
    cpFloat damping;
    int updateCounter;
    cpFloat behaviourRotation;
}

@property (nonatomic, assign) float makkuHealth;
@property (nonatomic, assign) float damping;

- (id)initWithPosition:(CGPoint)thePosition;
- (void)update;
- (void)collectedDust:(Dust *)dust;
- (void)goalReached;
- (void)wasCloned;
- (void)kill;
- (BOOL) applyDamage:(float) damage;
- (float)maxHealth;
- (void)setEffectsVolume:(float)value;

@end
