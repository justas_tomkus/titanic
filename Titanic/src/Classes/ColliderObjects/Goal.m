//
//  Goal.m
//  Titanic
//
//  Created by Philipp Anger on 03.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Goal.h"
#import "ModuleManager.h"

@implementation Goal

- (id)initWithCInfo:(ColliderInfo*)info {
    self = [super initWithPosition:cpv(info.x, info.y) width:info.width height:info.height andShapeType:ShapeTypeRectangle];
    if (self) {
        
        // PHYSICS
        body = [[ChipmunkBody alloc] initStaticBody];
        self.body.pos = cpv(self.x + info.width/2, self.y + info.height/2);
        
        //leftSide Shape
        ChipmunkShape *shape = [[ChipmunkStaticPolyShape alloc] initBoxWithBody:self.body width:info.width height:info.height];
        
        shape.collisionType = [Goal class];
        shape.data = self;
        
        [self.shapes addObject:shape];
        [shape release];
        // MODULES
        //adding to physics space
        [[ModuleManager sharedModuleManager].physicsModule addStaticBodyWithShapes:self.shapes];
        
        // COLLIDER-OBJECT STUFF
//        [self drawBoundingWithOffset:cpvzero andRightBottomOffset:cpv(info.width,info.height)];
    }
    return self;
}

- (void)dealloc
{
    [image release];
    [super dealloc];
}

@end
