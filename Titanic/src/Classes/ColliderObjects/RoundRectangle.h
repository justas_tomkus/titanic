//
//  Couch.h
//  Titanic
//
//  Created by Philipp Anger on 21.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
#import "Collider.h"
#import "ColliderInfo.h"

@interface RoundRectangle : Collider {
    SPImage *image;
}

- (id)initWithCInfo:(ColliderInfo*)info;

@end
