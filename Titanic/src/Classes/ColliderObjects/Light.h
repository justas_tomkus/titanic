//
//  Couch.h
//  Titanic
//
//  Created by Philipp Anger on 21.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
#import "Collider.h"
#import "ColliderInfo.h"
#import "Makku.h"

@interface Light : Collider {
    SPImage *image;
    float radius;
    float intensity;
}

- (id)initWithCInfo:(ColliderInfo*)info;
- (BOOL)applyLightDamage:(Makku*)makku withDeltaTime:(float) dt;

@property (nonatomic, assign) float intensity;

@end
