//
//  Goal.h
//  Titanic
//
//  Created by Philipp Anger on 03.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Collider.h"
#import "ColliderInfo.h"

@interface Goal : Collider {
    SPImage *image;
}

- (id)initWithCInfo:(ColliderInfo*)info;

@end
