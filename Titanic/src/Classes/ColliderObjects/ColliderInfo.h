//
//  ColliderInfo.h
//  Titanic
//
//  Created by Andy on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColliderInfo : NSObject {
    float elasticity;
    float friction;
    float x, y, with, height;
}

@property (nonatomic, assign) float x, y, width, height;
@property (nonatomic, assign) float elasticity;
@property (nonatomic, assign) float friction;

@end
