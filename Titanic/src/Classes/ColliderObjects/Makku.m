//
//  Makku.m
//  Titanic
//
//  Created by Philipp Anger on 05.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Makku.h"
#import "TiltModule.h"
#import "ModuleManager.h"
#import "GameNotifications.h"
#import "Dust.h"
#import "Constants.h"
#include <stdlib.h>

@interface Makku()
- (void) applyBehaviour;
- (void)onAddedToStage:(SPEvent *)event;
- (void)onRemovedFromStage:(SPEvent *)event;
- (void)checkMakkuStatus;
- (void)onMakkuClicked:(SPEvent *)event;
- (void)playMakkuAnimation;
@end

@implementation Makku

@synthesize makkuHealth;
@synthesize damping;

- (id)initWithPosition:(CGPoint)thePosition {
    self = [super initWithPosition:thePosition width:MAKKU_SIZE height:MAKKU_SIZE andShapeType:ShapeTypeCircle];
    if (self) {
        
        makkuHealth = MAKKU_SPAWN_HEALTH;
        
        // BEHAVIOUR
        updateCounter = 0;
        behaviourRotation = 1;
        
        
        // PHYSICS
        [self setupPhysics];

        
        // MOVIECLIPS
        SPTextureAtlas *makkuEyeColseAtlas = [SPTextureAtlas atlasWithContentsOfFile:@"makku-closeeyes.xml"];
        NSArray *frames = [makkuEyeColseAtlas texturesStartingWith:@"walk_"];
        closeEyesClip = [[SPMovieClip alloc] initWithFrames:frames fps:12];
        closeEyesClip.width = MAKKU_SIZE;
        closeEyesClip.height = MAKKU_SIZE;
        
        currentMovieClip = closeEyesClip;
        currentMovieClip.color = MAKKU_HEALTH_COLOR_NORMAL;
        currentMovieClip.loop = NO;
        [self addChild:currentMovieClip];
        
        int interval = ((arc4random() % (10-3)) + 3);   // animation with interval of randomly 3-10 seconds
        animationTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(playMakkuAnimation) userInfo:nil repeats:YES];
        
        // SOUNDS
//        touchedSounds = [[NSMutableSet alloc] init];
//        [touchedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuTouched1.aifc"]];
//        [touchedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuTouched2.aifc"]];
//        [touchedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuTouched3.aifc"]];
        
        hurtSounds = [[NSMutableSet alloc] init];
        [hurtSounds addObject:[SPSound soundWithContentsOfFile:@"makkuHurt_1.aifc"]];
        [hurtSounds addObject:[SPSound soundWithContentsOfFile:@"makkuHurt_2.aifc"]];
        [hurtSounds addObject:[SPSound soundWithContentsOfFile:@"makkuHurt_3.aifc"]];
        
//        diedSounds = [[NSMutableSet alloc] init];
//        [diedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuDied1.aifc"]];
//        [diedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuDied2.aifc"]];
//        [diedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuDied3.aifc"]];
        
        clonedSounds = [[NSMutableSet alloc] init];
//        [clonedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuCloned_1.aifc"]]; // buttonClicked Sound!!!
        [clonedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuCloned_2.aifc"]];
        [clonedSounds addObject:[SPSound soundWithContentsOfFile:@"makkuCloned_3.aifc"]];
        
        gatheredDustSounds = [[NSMutableSet alloc] init];
        [gatheredDustSounds addObject:[SPSound soundWithContentsOfFile:@"makkuGatheredDust_1.aifc"]];
        [gatheredDustSounds addObject:[SPSound soundWithContentsOfFile:@"makkuGatheredDust_2.aifc"]];
        [gatheredDustSounds addObject:[SPSound soundWithContentsOfFile:@"makkuGatheredDust_3.aifc"]];
        
        goalReachedSounds = [[NSMutableSet alloc] init];
        [goalReachedSounds addObject:[SPSound soundWithContentsOfFile:@"levelGoalReached_1.aifc"]];
        [goalReachedSounds addObject:[SPSound soundWithContentsOfFile:@"levelGoalReached_2.aifc"]];
        [goalReachedSounds addObject:[SPSound soundWithContentsOfFile:@"levelGoalReached_3.aifc"]];
        [goalReachedSounds addObject:[SPSound soundWithContentsOfFile:@"levelGoalReached_4.aifc"]];
        
        sneezeSounds = [[NSMutableSet alloc] init];
        [sneezeSounds addObject:[SPSound soundWithContentsOfFile:@"makkuSneeze_1.aifc"]];
        [sneezeSounds addObject:[SPSound soundWithContentsOfFile:@"makkuSneeze_2.aifc"]];
        [sneezeSounds addObject:[SPSound soundWithContentsOfFile:@"makkuSneeze_3.aifc"]];
        
        //TODO delete later, just for fun
        diedSounds = sneezeSounds;
        
        gigglingSounds = [[NSMutableSet alloc] init];
        [gigglingSounds addObject:[SPSound soundWithContentsOfFile:@"makkuGiggling_1.aifc"]];
        [gigglingSounds addObject:[SPSound soundWithContentsOfFile:@"makkuGiggling_2.aifc"]];
        [gigglingSounds addObject:[SPSound soundWithContentsOfFile:@"makkuGiggling_3.aifc"]];
        [gigglingSounds addObject:[SPSound soundWithContentsOfFile:@"makkuGiggling_4.aifc"]];
        
//        movesSounds = [[NSMutableSet alloc] init];
//        [movesSounds addObject:[SPSound soundWithContentsOfFile:@"makkuMoves1.aifc"]];
//        [movesSounds addObject:[SPSound soundWithContentsOfFile:@"makkuMoves2.aifc"]];
//        [movesSounds addObject:[SPSound soundWithContentsOfFile:@"makkuMoves3.aifc"]];
        
        effectVolume = 1.0;
        
        // COLLIDER-OBJECT STUFF
//        [self drawBoundingWithOffset:cpvzero andRightBottomOffset:cpv(MAKKU_SIZE, MAKKU_SIZE)];
        
        // EVENTLISTENERS
        [self addEventListener:@selector(onAddedToStage:) atObject:self forType:SP_EVENT_TYPE_ADDED_TO_STAGE];
        [self addEventListener:@selector(onRemovedFromStage:) atObject:self forType:SP_EVENT_TYPE_REMOVED_FROM_STAGE];
        [self addEventListener:@selector(onMakkuClicked:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    }
    return self;
}

- (void)dealloc
{
    // SOUNDS
    [soundChannel stop];
    [soundChannel release];
    [touchedSounds release];
    [hurtSounds release];
    [diedSounds release];
    [clonedSounds release];
    [gatheredDustSounds release];
    [goalReachedSounds release];
    [sneezeSounds release];
    [gigglingSounds release];
    [movesSounds release];
    
    // MOVIECLIPS
    [currentMovieClip stop];
    [currentMovieClip release];
    [closeEyesClip stop];    
    [closeEyesClip release];
    
    [animationTimer release];
    [super dealloc];
}

- (void)setEffectsVolume:(float)value {
    effectVolume = value;
}

- (void)playRandomSoundFromSet:(NSMutableSet*)set {
    [soundChannel release];
    soundChannel = [[[set anyObject] createChannel] retain];
    [soundChannel setVolume:effectVolume];
    [soundChannel play];
}

- (void)collectedDust:(Dust *)dust {
    if (![soundChannel isPlaying]) {
        [self playRandomSoundFromSet:gatheredDustSounds];
    }
    
    self.makkuHealth += dust.dirtiness;
    [self checkMakkuStatus];
}

- (void)wasCloned {
    if (![soundChannel isPlaying]) {
        [self playRandomSoundFromSet:clonedSounds];
    }
    
    makkuHealth = makkuHealth - MAKKU_SPAWN_HEALTH;
    [self checkMakkuStatus];
}

- (void)kill {
    if ([soundChannel isPlaying]) {
        [soundChannel stop];
    }
    [self playRandomSoundFromSet:diedSounds];

    
    [[ModuleManager sharedModuleManager].physicsModule removeDynamicBody:self.body andShapes:self.shapes];
    [self removeFromParent];
}

- (BOOL) applyDamage:(float) damage {
    self.makkuHealth -= damage;
    if (![soundChannel isPlaying]) {
        [self playRandomSoundFromSet:hurtSounds];
    }
    [self checkMakkuStatus];
    return NO;
}


#pragma mark - Private Methods

- (void)setupPhysics {
    cpFloat radius = self.width / 2 * 0.8;
    cpFloat moment = cpMomentForCircle(MAKKU_MASS, 0.0f, radius, cpv(0,0));
    body = [[ChipmunkBody alloc] initWithMass:MAKKU_MASS andMoment:moment];
    self.body.pos = cpv(self.x, self.y);
    
    //random damping valus
    int rand = arc4random() % 101;
    float alpha = rand / 100.0;
    self.damping = BEH_MIN_DAMPING * alpha + BEH_MAX_DAMPING * (1 - alpha);
    
    self.body.body->velocity_func = cpMakkuUpdateVelocity;
    self.body.body->data = self;
    
    ChipmunkShape *shape = [[ChipmunkCircleShape alloc] initWithBody:self.body radius:radius offset:cpv(0,0)];
    shape.elasticity = 0.5f;
    shape.friction = 1.0f;
    shape.collisionType = [Makku class];
    shape.data = self;
    [self.shapes addObject:shape];
    [shape release];
    
    [[ModuleManager sharedModuleManager].physicsModule addDynamicBody:self.body andShapes:self.shapes];
}

void cpMakkuUpdateVelocity(cpBody *body, cpVect gravity, cpFloat damping, cpFloat dt) {
    cpFloat newDamp = cpfpow(((Makku*)body->data).damping, dt);;
    
	body->v = cpvclamp(cpvadd(cpvmult(body->v, newDamp), cpvmult(cpvadd(gravity, cpvmult(body->f, body->m_inv)), dt)), body->v_limit);
    
	cpFloat w_limit = body->w_limit;
	body->w = cpfclamp(body->w*newDamp + body->t*body->i_inv*dt, -w_limit, w_limit);
    
	cpBodySanityCheck(body);
}

- (void) applyBehaviour {
    int randX = arc4random() % 101;
    int randY = arc4random() % 101;
    int randA = arc4random() % 101;
    
    cpFloat x = randX / 100.0 - 0.5, y = randY / 100.0 - 0.5, angle = randA / 100.0 - 0.5;
    angle *= behaviourRotation * BEH_ROTATION_INTENSITIY;
    updateCounter++;
    if (updateCounter > BEH_ROTATION_CHANGE_INTERVALL) {
        behaviourRotation *=signbit(x);
        updateCounter = 0;
    }
    [self.body applyImpulse:cpvmult(cpv(x,y),5.0) offset:cpvzero];
    self.body.angVel += angle;
}

- (void)onAddedToStage:(SPEvent *)event
{
    [self.stage.juggler addObject:closeEyesClip];
}

- (void)onRemovedFromStage:(SPEvent *)event
{
    [self.stage.juggler removeObject:closeEyesClip];
}

- (void)checkMakkuStatus {
    if (self.makkuHealth >= MAKKU_CLONE_HEALTH) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMakkuIsCloning object:self userInfo:nil];
    } else if (self.makkuHealth <= MAKKU_DEAD_HEALTH) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMakkuDied object:self userInfo:nil];
    } else {
        float healthRange = (MAKKU_CLONE_HEALTH - MAKKU_DEAD_HEALTH);
        
        if (self.makkuHealth < (healthRange * 0.2)) {
            currentMovieClip.color = MAKKU_HEALTH_COLOR_CRITICAL;
        } else if (self.makkuHealth < (healthRange * 0.4)) {
            currentMovieClip.color = MAKKU_HEALTH_COLOR_BAD;
        } else if (self.makkuHealth < (healthRange * 0.45)) {
            currentMovieClip.color = MAKKU_HEALTH_COLOR_HURT;
        } else if (self.makkuHealth > (healthRange * 0.8)) {
            currentMovieClip.color = MAKKU_HEALTH_COLOR_AWESOME;
        } else if (self.makkuHealth > (healthRange * 0.6)) {
            currentMovieClip.color = MAKKU_HEALTH_COLOR_BETTER;
        } else if (self.makkuHealth > (healthRange * 0.55)) {
            currentMovieClip.color = MAKKU_HEALTH_COLOR_GOOD;
        } else {
            currentMovieClip.color = MAKKU_HEALTH_COLOR_NORMAL;
        }
    }
}

- (void)onMakkuClicked:(SPEvent *)event {
    if (![soundChannel isPlaying]) {
        [self playRandomSoundFromSet:gigglingSounds];
    }
}

- (void)playMakkuAnimation {
    if (![currentMovieClip isPlaying]) {
        [currentMovieClip stop];
        [currentMovieClip play];
    }
}

- (void)goalReached {
    if ([soundChannel isPlaying]) {
        [soundChannel stop];
    }
    [self playRandomSoundFromSet:goalReachedSounds];}

float angleDistance(float a1, float a2) {
    float diff = fmodf(a2 - a1, 2 * PI);
    float abs = fabsf(diff);
    
    if (abs > 180) {
        if (diff < 0)
            diff = 2 * PI - abs;
        else
            diff = abs - 2 * PI;
    }
    return diff;
}
#pragma mark - Game Methods

- (void)update {
    static float alphaMax = 1000;
    if(!currentMovieClip.isPlaying) {
        if (rand() > 80) {
            [currentMovieClip play];
        }
    }

    self.position = CGPointMake(self.body.pos.x, self.body.pos.y);

    cpFloat lastAngle = self.body.angle;
    cpFloat dirAngle = cpvtoangle(self.body.vel) - PI_HALF;

    float alpha = cpvlength(self.body.vel);
    alpha = MIN(alpha, alphaMax);
    alpha /= alphaMax;

    self.body.angle = lastAngle * (1 - alpha) + dirAngle * alpha;
    
//    if (fabs(lastAngle - dirAngle) < PI_HALF / 9)
//        self.body.angle = dirAngle;
//    else {
//        cpFloat blah = signbit(lastAngle - dirAngle);
//        self.body.angle += blah * PI_HALF / 9;
//        //self.body.angle = lastAngle * (1 - 0.2 * 0.5) + dirAngle * (0.2*0.5);
//    }
//    self.body.angle = cpvtoangle(self.body.vel) - PI_HALF;
    self.rotation = self.body.angle;
    
    [self applyBehaviour];
}

- (float)maxHealth {
    return MAKKU_CLONE_HEALTH;
}

@end
