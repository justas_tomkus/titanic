//
//  Couch.m
//  Titanic
//
//  Created by Philipp Anger on 21.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Circle.h"
#import "ModuleManager.h"
#import "ColliderInfo.h"

@implementation Circle

- (id)initWithCInfo:(ColliderInfo*)info {
    self = [super initWithPosition:cpv(info.x, info.y) width:info.width height:info.height andShapeType:ShapeTypeCircle];
    if (self) {
        
        // PHYSICS
        body = [[ChipmunkBody alloc] initStaticBody];
        self.body.pos = cpv(self.x, self.y);
        
        //leftSide Shape
        ChipmunkShape *shape = [[ChipmunkStaticCircleShape alloc] initWithBody:self.body radius:info.width/2 offset:cpvzero];
        
        shape.collisionType = (id)TypeSofa;
        shape.elasticity = info.elasticity;
        shape.friction = info.friction;
        
        [self.shapes addObject:shape];
        [shape release];
        // MODULES
        //adding to physics space
        [[ModuleManager sharedModuleManager].physicsModule addStaticBodyWithShapes:self.shapes];
        
        // COLLIDER-OBJECT STUFF
//        [self drawBoundingWithOffset:cpvzero andRightBottomOffset:cpv(info.width,info.height)];
    }
    return self;
}

- (void)dealloc
{
    [image release];
    [super dealloc];
}


@end
