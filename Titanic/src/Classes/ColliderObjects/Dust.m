//
//  Dust.m
//  Titanic
//
//  Created by Philipp Anger on 03.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Dust.h"
#import "ModuleManager.h"
#import "Constants.h"

@implementation Dust

@synthesize collected;
@synthesize dirtiness;

- (id)initWithPosition:(CGPoint)thePosition andDirtiness:(float)theDirtiness {
    self = [super initWithPosition:thePosition width:DUST_SIZE height:DUST_SIZE andShapeType:ShapeTypeCircle];
    if (self) {
        
        collected = NO;
        
        // PHYSICS
        //cpFloat moment = cpMomentForCircle(DUST_MASS, 0.0f, DUST_SIZE/2, cpv(0,0));
        body = [[ChipmunkBody alloc] initStaticBody];
        //body = [[ChipmunkBody alloc] initWithMass:DUST_MASS andMoment:moment];
        self.body.pos = cpv(self.x, self.y);
        self.dirtiness = theDirtiness;
        
        ChipmunkShape *shape = [[ChipmunkStaticCircleShape alloc] initWithBody:self.body radius:DUST_SIZE/2 offset:cpv(0,0)];
        shape.elasticity = 1.0f;
        shape.friction = 1.0f;
        shape.collisionType = [Dust class];
        shape.data = self;
        [self.shapes addObject:shape];
        [shape release];
        
        [[ModuleManager sharedModuleManager].physicsModule addStaticBodyWithShape:shape];
        
        // IMAGE STUFF
        image = [[SPImage alloc] initWithContentsOfFile:@"dust1-2.png"];
        image.width = DUST_SIZE;
        image.height = DUST_SIZE;
        image.color = 0xaaaaaa - (self.dirtiness * 0x222222);
        [self addChild:image];
        
    }
    
    return self;
}

- (void)dealloc
{
    [image release];
    [super dealloc];
}

- (void)wasCollected {
    collected = YES;
    [[ModuleManager sharedModuleManager].physicsModule removeStaticBodyWithShape:[self.shapes lastObject]];
    [self removeFromParent];
}

@end
