//
//  Couch.m
//  Titanic
//
//  Created by Philipp Anger on 21.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Light.h"
#import "ModuleManager.h"
#import "ColliderInfo.h"
#import "GameNotifications.h"

@implementation Light

@synthesize intensity;

- (id)initWithCInfo:(ColliderInfo*)info {
    self = [super initWithPosition:cpv(info.x, info.y) width:info.width height:info.height andShapeType:ShapeTypeCircle];
    if (self) {
        
        // PHYSICS
        body = [[ChipmunkBody alloc] initStaticBody];
        self.body.pos = cpv(self.x, self.y);
        
        radius = info.width/2;
        //leftSide Shape
        ChipmunkShape *shape = [[ChipmunkStaticCircleShape alloc] initWithBody:self.body radius:radius offset:cpvzero];
        shape.sensor = YES;
        
        shape.collisionType = [Light class];
        shape.data = self;
        
        [self.shapes addObject:shape];
        [shape release];
        // MODULES
        //adding to physics space
        [[ModuleManager sharedModuleManager].physicsModule addStaticBodyWithShapes:self.shapes];
        
        // COLLIDER-OBJECT STUFF
//        [self drawBoundingWithOffset:cpvzero andRightBottomOffset:cpv(info.width,info.height)];
    }
    return self;
}

- (void)dealloc
{
    [image release];
    [super dealloc];
}

- (BOOL)applyLightDamage:(Makku*)makku withDeltaTime:(float) dt{
    //ChipmunkShape *light = shapes.lastObject;
    float distance = fmaxf(0, cpvlength(cpvsub(self.body.pos, makku.body.pos)) - makku.radius);
    float halfRadius = radius / 2;
    float damage = dt * [makku maxHealth] * intensity;
    BOOL result = NO;
    
    if (distance < halfRadius) {
        result = [makku applyDamage:damage];
    } else {
        distance -= halfRadius;
        float alpha = distance / halfRadius;
        float lowDamage = (1-alpha) * damage + alpha * damage/10;
        result = [makku applyDamage:lowDamage];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyLightDamage object:nil];
    
    return result;
}

@end
