//
//  ColliderInfo.m
//  Titanic
//
//  Created by Andy on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "ColliderInfo.h"

@implementation ColliderInfo

@synthesize x, y, width, height, elasticity, friction;

@end
