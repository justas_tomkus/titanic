//
//  NSNotificationCenter+UserObject.m
//  runtastic
//
//  Created by Stefan Damm on 29.04.11.
//  Copyright 2011 runtastic GmbH. All rights reserved.
//

#import "NSNotificationCenter+UserObject.h"


@implementation NSNotificationCenter (UserObject)

- (void)postNotificationName:(NSString *)notificationName object:(id)notificationSender userObject:(id) userObject {
    NSDictionary *userInfo = nil;
    
    if (userObject) {
        userInfo = [NSDictionary dictionaryWithObject:userObject forKey:@"userObject"];
    }
    
    [self postNotificationName:notificationName object:notificationSender userInfo:userInfo];
}

- (void)postNotificationNameOnMainThread:(NSString *)notificationName {
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(postNotificationNameOnMainThread:) withObject:notificationName waitUntilDone:YES];
        return;
    }
    
    [self postNotificationName:notificationName object:nil];
}

@end
