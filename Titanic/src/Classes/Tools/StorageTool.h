//
//  StorageModule.h
//  Titanic
//
//  Created by MaxL on 26.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StorageTool : NSObject

+ (void)saveScore:(int)score forLevel:(int)lvlNumber withTimeInSec:(float)timeInSec maxDust:(int)maxDust collectedDust:(int)colDust limitDustPercentage:(float)limit nrOfDeadMakkus:(int)deadMakkusCt andNrOfRescuedMakkus:(int)rescuedMakkusCt;
+ (NSDictionary *)getScoreForLevel:(int)lvlNumber;
+ (NSDictionary *)getScoreForAllLevels;

@end
