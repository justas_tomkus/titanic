//
//  UIAlertView+DisableAlerts.h
//  Titanic
//
//  Created by MaxL on 24.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (DisableAlerts)

@end
