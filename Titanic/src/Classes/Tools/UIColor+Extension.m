//
//  UIColor+Extension.m
//  GeoGame
//
//  Created by MaxL on 14.12.11.
//  Copyright (c) 2011 DiamondDev. All rights reserved.
//

#import "UIColor+Extension.h"


@implementation UIColor (Extension)

+ (UIColor *)colorWithHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [UIColor colorComponentFromHexString: colorString start: 0 length: 1];
            green = [UIColor colorComponentFromHexString: colorString start: 1 length: 1];
            blue  = [UIColor colorComponentFromHexString: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [UIColor colorComponentFromHexString: colorString start: 0 length: 1];
            red   = [UIColor colorComponentFromHexString: colorString start: 1 length: 1];
            green = [UIColor colorComponentFromHexString: colorString start: 2 length: 1];
            blue  = [UIColor colorComponentFromHexString: colorString start: 3 length: 1];          
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [UIColor colorComponentFromHexString: colorString start: 0 length: 2];
            green = [UIColor colorComponentFromHexString: colorString start: 2 length: 2];
            blue  = [UIColor colorComponentFromHexString: colorString start: 4 length: 2];                      
            break;
        case 8: // #AARRGGBB
            alpha = [UIColor colorComponentFromHexString: colorString start: 0 length: 2];
            red   = [UIColor colorComponentFromHexString: colorString start: 2 length: 2];
            green = [UIColor colorComponentFromHexString: colorString start: 4 length: 2];
            blue  = [UIColor colorComponentFromHexString: colorString start: 6 length: 2];                      
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (uint)uIntWithHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [UIColor colorComponentFromHexString: colorString start: 0 length: 1];
            green = [UIColor colorComponentFromHexString: colorString start: 1 length: 1];
            blue  = [UIColor colorComponentFromHexString: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [UIColor colorComponentFromHexString: colorString start: 0 length: 1];
            red   = [UIColor colorComponentFromHexString: colorString start: 1 length: 1];
            green = [UIColor colorComponentFromHexString: colorString start: 2 length: 1];
            blue  = [UIColor colorComponentFromHexString: colorString start: 3 length: 1];          
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [UIColor colorComponentFromHexString: colorString start: 0 length: 2];
            green = [UIColor colorComponentFromHexString: colorString start: 2 length: 2];
            blue  = [UIColor colorComponentFromHexString: colorString start: 4 length: 2];                      
            break;
        case 8: // #AARRGGBB
            alpha = [UIColor colorComponentFromHexString: colorString start: 0 length: 2];
            red   = [UIColor colorComponentFromHexString: colorString start: 2 length: 2];
            green = [UIColor colorComponentFromHexString: colorString start: 4 length: 2];
            blue  = [UIColor colorComponentFromHexString: colorString start: 6 length: 2];                      
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    
    return (uint)((255 << 24) + (((int)(red * 255)) << 16) + (((int)(green * 255)) << 8) + ((int)(blue * 255)));
}

+ (CGFloat) colorComponentFromHexString: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end
