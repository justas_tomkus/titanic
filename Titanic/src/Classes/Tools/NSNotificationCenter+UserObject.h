//
//  NSNotificationCenter+UserObject.h
//  runtastic
//
//  Created by Stefan Damm on 29.04.11.
//  Copyright 2011 runtastic GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSNotificationCenter (UserObject)

- (void)postNotificationName:(NSString *)notificationName object:(id)notificationSender userObject:(id)userObject;

- (void)postNotificationNameOnMainThread:(NSString *)notificationName;

@end
