//
//  UIColor+Extension.h
//  GeoGame
//
//  Created by MaxL on 14.12.11.
//  Copyright (c) 2011 DiamondDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)

+ (UIColor *)colorWithHexString: (NSString *) hexString;
+ (uint)uIntWithHexString: (NSString *) hexString;
+ (CGFloat) colorComponentFromHexString:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length;

@end
