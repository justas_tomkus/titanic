//
//  NSNotification+UserObject.m
//  runtastic
//
//  Created by Stefan Damm on 29.04.11.
//  Copyright 2011 runtastic GmbH. All rights reserved.
//

#import "NSNotification+UserObject.h"


@implementation NSNotification (UserObject)

- (id)userObject {
    return [[self userInfo] objectForKey:@"userObject"];
}

@end
