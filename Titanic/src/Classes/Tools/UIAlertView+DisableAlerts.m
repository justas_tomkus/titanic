//
//  UIAlertView+DisableAlerts.m
//  Titanic
//
//  Created by MaxL on 24.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "UIAlertView+DisableAlerts.h"

@implementation UIAlertView (DisableAlerts)

- (void)show {
    // don't show
    [self dismissWithClickedButtonIndex:-1 animated:NO];
}

@end
