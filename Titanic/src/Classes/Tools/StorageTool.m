//
//  StorageModule.m
//  Titanic
//
//  Created by MaxL on 26.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "StorageTool.h"

@implementation StorageTool

+ (void)saveScore:(int)score forLevel:(int)lvlNumber withTimeInSec:(float)timeInSec maxDust:(int)maxDust collectedDust:(int)colDust limitDustPercentage:(float)limit nrOfDeadMakkus:(int)deadMakkusCt andNrOfRescuedMakkus:(int)rescuedMakkusCt {
    NSMutableDictionary *lvlDict = [NSMutableDictionary dictionaryWithCapacity:3];
    
    [lvlDict setObject:[NSNumber numberWithInt:score] forKey:@"score"];
    [lvlDict setObject:[NSNumber numberWithFloat:timeInSec] forKey:@"timeInSeconds"];
    [lvlDict setObject:[NSNumber numberWithInt:maxDust] forKey:@"maximumDust"];
    [lvlDict setObject:[NSNumber numberWithInt:colDust] forKey:@"collectedDust"];
    [lvlDict setObject:[NSNumber numberWithFloat:limit] forKey:@"limitDustPercentage"];
    [lvlDict setObject:[NSNumber numberWithInt:deadMakkusCt] forKey:@"deadMakkusCount"];
    [lvlDict setObject:[NSNumber numberWithInt:rescuedMakkusCt] forKey:@"rescuedMakkusCount"];
    
    [[NSUserDefaults standardUserDefaults] setObject:lvlDict forKey:[NSString stringWithFormat:@"Level:%d", lvlNumber]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDictionary *)getScoreForLevel:(int)lvlNumber {
    return [[NSUserDefaults standardUserDefaults] dictionaryForKey:[NSString stringWithFormat:@"Level:%d", lvlNumber]];
}

+ (NSDictionary *)getScoreForAllLevels {
    
    NSMutableDictionary *allLevels = [NSMutableDictionary dictionary];
    
    int lvlNr = 1;
    
    NSDictionary *lvlDict = [StorageTool getScoreForLevel:lvlNr];
    
    while (lvlDict != nil) {
        [allLevels setObject:lvlDict forKey:[NSString stringWithFormat:@"Level:%d", lvlNr]];
        lvlDict = [StorageTool getScoreForLevel:++lvlNr];
    }
    
    return allLevels;
}

@end
