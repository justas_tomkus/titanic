//
//  UIImage+ColorReplacing.h
//  Titanic
//
//  Created by MaxL on 04.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColorReplacing)
- (UIImage *)imageWithReplacingColor:(UIColor *)repColor forColor:(UIColor *)origColor;
@end
