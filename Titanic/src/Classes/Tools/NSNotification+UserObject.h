//
//  NSNotification+UserObject.h
//  runtastic
//
//  Created by Stefan Damm on 29.04.11.
//  Copyright 2011 runtastic GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSNotification (UserObject)

- (id)userObject;

@end
