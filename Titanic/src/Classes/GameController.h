//
//  GameController.h
//  AppScaffold
//

#import "SPStage.h"
#import "GameState.h"
#import "PlayState.h"
#import "MenuState.h"
#import "ModuleManager.h"

@interface GameController : SPStage
{
  @private
    
    GameState *currentState;
    
    PlayState *playState;
    MenuState *menuState;
    
    float screenWidth;
    float screenHeight;
}

@property (nonatomic, strong) GameState *currentState;
@property (nonatomic, strong) PlayState *playState;
@property (nonatomic, strong) MenuState *menuState;

- (void)rotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
                       animationTime:(double)time;

@end
