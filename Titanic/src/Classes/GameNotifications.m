//
//  GameNotifications.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "GameNotifications.h"

// MENU
NSString *kNotifyShowMenu = @"kNotifyShowMenu";
NSString *kNotifyShowMainMenu = @"kNotifyShowMainMenu";
NSString *kNotifyShowPlayMenu = @"kNotifyShowPlayMenu";
NSString *kNotifyShowOptionsMenu = @"kNotifyShowOptionsMenu";
NSString *kNotifyShowCreditsMenu = @"kNotifyShowCreditsMenu";
NSString *kNotifyShowHighscoreMenu = @"kNotifyShowHighscoreMenu";

// PLAY
NSString *kNotifyShowLevelWithNumber = @"kNotifyShowLevelWithNumber";
NSString *kNotifyMakkuIsCloning = @"kNotifyMakkuIsCloning";
NSString *kNotifyMakkuDied = @"kNotifyMakkuDied";
NSString *kNotifyDustCollected = @"kNotifyDustCollected";
NSString *kNotifyDustBarUpdate = @"kNotifyDustBarUpdate";
NSString *kNotifyGoalCollision = @"kNotifyGoalCollision";
NSString *kNotifyLightDamage = @"kNotifyLightCollision";
NSString *kNotifyGameOver = @"kNotifyGameOver";
NSString *kNotifyShowArrowWithGoal = @"kNotifyShowArrowWithGoal";
NSString *kNotifyHideArrow = @"kNotifyHideArrow";

// GAMEPLAY
NSString *kNotifyStartLevel = @"kNotifyStartLevel";
NSString *kNotifyPauseLevel = @"kNotifyPauseLevel";
NSString *kNotifyResumeLevel = @"kNotifyResumeLevel";
NSString *kNotifyRetryLevel = @"kNotifyRetryLevel";
NSString *kNotifyCompleteLevel = @"kNotifyCompleteLevel";
NSString *kNotifyQuitLevel = @"kNotifyQuitLevel";
NSString *kNotifyNextLevel = @"kNotifyNextLevel";

// SOUND
NSString *kNotifySetEffectVolume = @"kNotifySetEffectVolume";
NSString *kNotifySetMusicVolume = @"kNotifySetMusicVolume";