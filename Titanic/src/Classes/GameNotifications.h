//
//  GameNotifications.h
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"

// MENU
extern NSString *kNotifyShowMenu;
extern NSString *kNotifyShowMainMenu;
extern NSString *kNotifyShowPlayMenu;
extern NSString *kNotifyShowOptionsMenu;
extern NSString *kNotifyShowCreditsMenu;
extern NSString *kNotifyShowHighscoreMenu;

// PLAY
extern NSString *kNotifyShowLevelWithNumber;
extern NSString *kNotifyMakkuIsCloning;
extern NSString *kNotifyMakkuDied;
extern NSString *kNotifyDustCollected;
extern NSString *kNotifyDustBarUpdate;
extern NSString *kNotifyGoalCollision;
extern NSString *kNotifyLightDamage;
extern NSString *kNotifyGameOver;
extern NSString *kNotifyShowArrowWithGoal;
extern NSString *kNotifyHideArrow;

// GAMEPLAY
extern NSString *kNotifyStartLevel;
extern NSString *kNotifyPauseLevel;
extern NSString *kNotifyResumeLevel;
extern NSString *kNotifyRetryLevel;
extern NSString *kNotifyCompleteLevel;
extern NSString *kNotifyQuitLevel;
extern NSString *kNotifyNextLevel;

// SOUND
extern NSString *kNotifySetEffectVolume;
extern NSString *kNotifySetMusicVolume;
