//
//  Collider.m
//  Titanic
//
//  Created by Philipp Anger on 16.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "Collider.h"
#import "ModuleManager.h"


@interface Collider() {
    float staticWidth;
    float staticHeight;
    
    NSMutableArray *bbList;
    NSMutableArray *positionList;
}

@end


@implementation Collider

@synthesize type;
@synthesize body;
@synthesize shapes;
@synthesize width;
@synthesize height;
@synthesize radius;

- (id)initWithPosition:(CGPoint)thePosition width:(float)theWidth height:(float)theHeight andShapeType:(ShapeType)theType
{
    self = [super init];
    if (self) {
        
        staticWidth = theWidth;
        staticHeight = theHeight;
        
        shapes = [[NSMutableArray alloc] init];
        
        self.position = thePosition;
        self.type = theType;
        self.body = nil;
        
        if (self.type == ShapeTypeCircle) {
            self.radius = theWidth / 2;
            self.pivotX = self.width / 2.0;
            self.pivotY = self.height / 2.0;
            self.x = self.position.x + self.width / 2.0;
            self.y = self.position.y + self.height / 2.0;
        }
    }
    return self;
}

- (void)dealloc
{
    [body release];
    [shapes release];
    [bbList release];
    [positionList release];
    [super dealloc];
}

#pragma mark - Bounding Box

- (void)drawBoundingWithOffset:(cpVect)theOffset andRightBottomOffset:(cpVect) theRightBottomVec {
#ifndef DNDEBUG
    cpFloat tWidth = theRightBottomVec.x, tHeight = theRightBottomVec.y;
    
    SPQuad *lineTop = [[[SPQuad alloc]initWithWidth:tWidth height:1 color:0xFF0000] autorelease];
    SPQuad *lineRight = [[[SPQuad alloc]initWithWidth:1 height:tHeight color:0xFF0000] autorelease];
    SPQuad *lineBottom = [[[SPQuad alloc]initWithWidth:tWidth height:1 color:0xFF0000] autorelease];
    SPQuad *lineLeft = [[[SPQuad alloc]initWithWidth:1 height:tHeight color:0xFF0000] autorelease];
    
    cpFloat x = theOffset.x, y = theOffset.y;
    
    lineTop.x = x;
    lineTop.y = y;
    lineRight.x = x + tWidth;
    lineRight.y = y ;
    lineBottom.x = x;
    lineBottom.y = y + tHeight;
    lineLeft.x = x;
    lineLeft.y = y;
    
    SPQuad *origin = [[[SPQuad alloc] initWithWidth:2 height:2 color:0x00FF00] autorelease];
    origin.x = x;
    origin.y = y;
    
    [bbList addObject:lineTop];
    [bbList addObject:lineRight];
    [bbList addObject:lineBottom];
    [bbList addObject:lineLeft];
    [bbList addObject:origin];
    
    [self addChild:lineTop];
    [self addChild:lineRight];
    [self addChild:lineBottom];
    [self addChild:lineLeft];
    [self addChild:origin];
#endif
}

#pragma mark - Getters and Setters

- (void)setPosition:(CGPoint)thePosition {
    self.x = thePosition.x;
    self.y = thePosition.y;
}

- (CGPoint)position {
    return CGPointMake(self.x, self.y);
}

- (float)width {
    return staticWidth;
}

- (float)height {
    return staticHeight;
}

@end
