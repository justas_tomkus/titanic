//
//  Collider.h
//  Titanic
//
//  Created by Philipp Anger on 16.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhysicsModule.h"
#import "ObjectiveChipmunk.h"

typedef enum {
    ShapeTypeCircle,
    ShapeTypeRectangle,
} ShapeType;

@interface Collider : SPSprite {
    ChipmunkBody *body;
    NSMutableArray *shapes;
}

@property (nonatomic,assign) ShapeType type;
@property (nonatomic,retain) ChipmunkBody *body;
@property (nonatomic,retain) NSMutableArray *shapes;
@property (nonatomic,assign) float width;
@property (nonatomic,assign) float height;
@property (nonatomic,assign) float radius;

- (id)initWithPosition:(CGPoint)thePosition width:(float)theWidth height:(float)theHeight andShapeType:(ShapeType)theType;
- (void)drawBoundingWithOffset:(cpVect)theOffset andRightBottomOffset:(cpVect) theRightBottomVec;

- (void)setPosition:(CGPoint)thePosition;
- (CGPoint)position;

@end
