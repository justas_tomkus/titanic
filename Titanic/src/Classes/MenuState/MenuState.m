//
//  MenuState.m
//  Titanic
//
//  Created by Philipp Anger on 02.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "MenuState.h"
#import "GameNotifications.h"

@interface MenuState()
- (void)registerNotifications;
- (void)showMainMenu:(NSNotification *)notification;
- (void)showPlayMenu:(NSNotification *)notification;
- (void)showHighscoreMenu:(NSNotification *)notification;
- (void)showMainMenu:(NSNotification *)notification;
- (void)showCreditsMenu:(NSNotification *)notification;
- (void)startLevel:(NSNotification *)notification;
@end

@implementation MenuState

@synthesize background;
@synthesize currentView;
@synthesize main;
@synthesize levels;
@synthesize options;
@synthesize highscore;
@synthesize credits;

- (id)initWithWidth:(float)theWidth andHeight:(float)theHeight {
    self = [super initWithWidth:theWidth andHeight:theHeight];
    if (self) {
        
        height = theHeight;
        width = theWidth;
        
        SPSound *music = [[SPSound alloc] initWithContentsOfFile:@"menuMusic.aifc"];
        soundChannel = [[music createChannel] retain];
        soundChannel.volume = MUSIC_VOLUME;
        soundChannel.loop = YES;
        
        [music release];
        
        [self registerNotifications];
    }
    return self;
}

- (void)dealloc
{
    [soundChannel stop];
    [soundChannel release];
    
    [background release];
    [currentView release];
    [main release];
    [levels release];
    [options release];
    [highscore release];
    [credits release];
    [super dealloc];
}

- (MainMenuView *)main {
    if (main == nil) {
        main = [[MainMenuView alloc] init];
    }
    return main;
}

- (LevelsMenuView *)levels {
    if (levels == nil) {
        levels = [[LevelsMenuView alloc] init];
    }
    return levels;
}

- (OptionsMenuView *)options {
    if (options == nil) {
        options = [[OptionsMenuView alloc] init];
    }
    return options;
}

- (HighscoreMenuView *)highscore {
    if (highscore == nil) {
        highscore = [[HighscoreMenuView alloc] init];
    }
    return highscore;
}

- (CreditsMenuView *)credits {
    if (credits == nil) {
        credits = [[CreditsMenuView alloc] init];
    }
    return credits;
}

- (void)stateWillShow {
    if (!background) {
        self.background = [[SPImage alloc] initWithContentsOfFile:@"background_menuscreen.png"];
        background.width = width;
        background.height = height;
        [self addChild:background];    
    }
}

- (void)stateDidHide {
    
}


#pragma mark - Notifications

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMainMenu:) name:kNotifyShowMainMenu object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPlayMenu:) name:kNotifyShowPlayMenu object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showHighscoreMenu:) name:kNotifyShowHighscoreMenu object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showOptionsMenu:) name:kNotifyShowOptionsMenu object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showCreditsMenu:) name:kNotifyShowCreditsMenu object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startLevel:) name:kNotifyShowLevelWithNumber object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setMusicVolume:) name:kNotifySetMusicVolume object:nil];
}

- (void)showMenu {
    if (![soundChannel isPlaying]) {
        [soundChannel play];
    }
    
    [self showMainMenu:nil];
}

- (void)showMainMenu:(NSNotification *)notification {
    [self removeChild:self.currentView];
    [self.currentView viewDidHide];
    
    self.currentView = self.main;
    
    [self.currentView viewWillShow];
    [self addChild:self.currentView];
}

- (void)showPlayMenu:(NSNotification *)notification {
    [self removeChild:self.currentView];
    [self.currentView viewDidHide];
    
    [self.levels updateForUserStatus];
    self.currentView = self.levels;
    
    [self.currentView viewWillShow];
    [self addChild:self.currentView];
}

- (void)showHighscoreMenu:(NSNotification *)notification {
    [self removeChild:self.currentView];
    [self.currentView viewDidHide];
    
    self.currentView = self.highscore;
    
    [self.currentView viewWillShow];
    [self addChild:self.currentView];
}

- (void)showOptionsMenu:(NSNotification *)notification {
    [self removeChild:self.currentView];
    [self.currentView viewDidHide];
    
    self.currentView = self.options;
    
    [self.currentView viewWillShow];
    [self addChild:self.currentView];
}

- (void)showCreditsMenu:(NSNotification *)notification {
    [self removeChild:self.currentView];
    [self.currentView viewDidHide];
    
    self.currentView = self.credits;
    
    [self.currentView viewWillShow];
    [self addChild:self.currentView];
}

- (void)startLevel:(NSNotification *)notification {
    if ([soundChannel isPlaying]) {
        [soundChannel stop];
    }
}

- (void)setMusicVolume:(NSNotification*)notification {
    NSNumber *value = notification.object;
    musicVolume = value.floatValue;
    [soundChannel setVolume:musicVolume];
}
         
@end
