//
//  CreditsMenuView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "CreditsMenuView.h"
#import "GameNotifications.h"

@interface CreditsMenuView()
-(void)onBackButtonTriggered:(SPEvent *)event;
@end

@implementation CreditsMenuView

@synthesize backButton;
@synthesize titleImage;

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    [backButton release];
    [titleImage release];
    [super dealloc];
}


#pragma mark - Trigger Events

- (void)onBackButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowMainMenu object:nil];   
}

#pragma mark - View Management

- (void)viewWillShow {
    if (!self.backButton) {
        SPTextureAtlas *buttonAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"menu_buttons.xml"];
        SPTextureAtlas *titleAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"menu_titles.xml"];
        
        backButton = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"backButton"] downState:[buttonAtlas textureByName:@"backButton_down"]];
        backButton.x = 240 - (backButton.width / 2);
        backButton.y = 240;
        
        titleImage = [[SPImage alloc] initWithTexture:[titleAtlas textureByName:@"title_credits"]];
        
        // ADD CHILDS
        [self addChild:titleImage];
        [self addChild:backButton];
        
        // EVENT LISTENERS
        [backButton addEventListener:@selector(onBackButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        
        [buttonAtlas release];
        [titleAtlas release];
    }
}

- (void)viewDidHide {
    [self removeAllChildren];
    self.backButton = nil;
    self.titleImage = nil;
}

@end
