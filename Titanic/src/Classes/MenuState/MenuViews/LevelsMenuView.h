//
//  LevelsMenuView.h
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"
#import "ViewManagement.h"

@interface LevelsMenuView : SPSprite <ViewManagement> {
    SPButton *level1Button;
    SPButton *level2Button;
    SPButton *level3Button;
    SPButton *level4Button;
    SPButton *backButton;
    SPImage *titleImage;
}

@property (nonatomic, strong) SPButton *level1Button;
@property (nonatomic, strong) SPButton *level2Button;
@property (nonatomic, strong) SPButton *level3Button;
@property (nonatomic, strong) SPButton *level4Button;
@property (nonatomic, strong) SPButton *backButton;
@property (nonatomic, strong) SPImage *titleImage;

- (void)updateForUserStatus;

@end
