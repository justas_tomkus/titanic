//
//  ViewManagement.h
//  Titanic
//
//  Created by MaxL on 27.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ViewManagement <NSObject>
- (void)viewWillShow;
- (void)viewDidHide;
@end
