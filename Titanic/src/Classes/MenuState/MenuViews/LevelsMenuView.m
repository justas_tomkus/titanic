//
//  LevelsMenuView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH. All rights reserved.
//

#import "LevelsMenuView.h"
#import "GameNotifications.h"
#import "ModuleManager.h"

@interface LevelsMenuView()
-(void)onLevelButtonTriggered:(SPEvent *)event;
-(void)onBackButtonTriggered:(SPEvent *)event;
@end

@implementation LevelsMenuView

@synthesize level1Button;
@synthesize level2Button;
@synthesize level3Button;
@synthesize level4Button;
@synthesize backButton;
@synthesize titleImage;

- (id)init
{
    self = [super init];
    if (self) {
                
    }
    return self;
}

- (void)dealloc
{
    [level1Button release];
    [level2Button release];
    [level3Button release];
    [level4Button release];
    [backButton release];
    [titleImage release];
    [super dealloc];
}


#pragma mark - Private Methods

- (void)updateForUserStatus {
    
    int highestUnlockedLevel = 1 + [[ModuleManager sharedModuleManager].levelModule getHighestUnlockedLevelNumber];
    
    switch (highestUnlockedLevel-1) {
        case 4:
            level4Button.enabled = YES;
            level4Button.alpha = 1;
        case 3:
            level3Button.enabled = YES;
            level3Button.alpha = 1;
        case 2:
            level2Button.enabled = YES;
            level2Button.alpha = 1;
        case 1:
            level1Button.enabled = YES;
            level1Button.alpha = 1;
        default:
            break;
    }

    
    switch (highestUnlockedLevel) {
        case 0:
        case 1:
        case 2:
            level2Button.enabled = NO;
            level2Button.alpha = 0.75;
            
        case 3:
            level3Button.enabled = NO;
            level3Button.alpha = 0.75;
            
        case 4:
            level4Button.enabled = NO;
            level4Button.alpha = 0.75;
        default:
            break;
    }
}


#pragma mark - Trigger Events

- (void)onLevelButtonTriggered:(SPEvent *)event {
    NSNumber *levelNumber = [NSNumber numberWithInt:[((SPButton *) event.target).name intValue]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowLevelWithNumber object:levelNumber];  
}

- (void)onBackButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowMainMenu object:nil];   
}


#pragma mark - View Management

- (void)viewWillShow {
    if (self.level1Button == nil) {
        SPTextureAtlas *buttonAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"menu_buttons.xml"];
        SPTextureAtlas *titleAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"menu_titles.xml"];
        
        level1Button = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"levelButton_1"] downState:[buttonAtlas textureByName:@"levelButton_1_down"]];
        level1Button.x = 50;
        level1Button.y = 175;
        level1Button.name = @"1";
        
        level2Button = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"levelButton_2"] downState:[buttonAtlas textureByName:@"levelButton_2_down"]];
        level2Button.x = 150;
        level2Button.y = 175;
        level2Button.name = @"2";
        
        level3Button = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"levelButton_3"] downState:[buttonAtlas textureByName:@"levelButton_3_down"]];
        level3Button.x = 250;
        level3Button.y = 175;
        level3Button.name = @"3";
        
        level4Button = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"levelButton_4"] downState:[buttonAtlas textureByName:@"levelButton_4_down"]];
        level4Button.x = 350;
        level4Button.y = 175;
        level4Button.name = @"4";
        
        backButton = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"backButton"] downState:[buttonAtlas textureByName:@"backButton_down"]];
        backButton.x = 240 - (backButton.width / 2);
        backButton.y = 260;
        
        titleImage = [[SPImage alloc] initWithTexture:[titleAtlas textureByName:@"title_levels"]];
        
        // ADD CHILDS
        [self addChild:titleImage];
        [self addChild:level1Button];
        [self addChild:level2Button];
        [self addChild:level3Button];
        [self addChild:level4Button];
        [self addChild:backButton];
        
        // EVENT LISTENERS
        [level1Button addEventListener:@selector(onLevelButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [level2Button addEventListener:@selector(onLevelButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [level3Button addEventListener:@selector(onLevelButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        
        [level4Button addEventListener:@selector(onLevelButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [backButton addEventListener:@selector(onBackButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        
        [buttonAtlas release];
        [titleAtlas release];
        
        [self updateForUserStatus];
    }
}

- (void)viewDidHide {
    [self removeAllChildren];
    self.titleImage = nil;
    self.level1Button = nil;
    self.level2Button = nil;
    self.level3Button = nil;
    self.level4Button = nil;
    self.backButton = nil;
}


@end
