//
//  OptionsMenuView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "OptionsMenuView.h"
#import "GameNotifications.h"

@interface OptionsMenuView()
-(void)onBackButtonTriggered:(SPEvent *)event;
@end

@implementation OptionsMenuView

@synthesize backButton;
@synthesize titleImage;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    [backButton release];
    [musicVolumeDesc release];
    [effectsVolumeDesc release];
    [muteMusicButton release];
    [unmuteMusicButton release];
    [muteEffectsButton release];
    [unmuteEffectsButton release];
    [incMusicVolumeButton release];
    [decMusicVolumeButton release];
    [incEffectsVolumeButton release];
    [decEffectsVolumeButton release];
    [titleImage release];
    [super dealloc];
}


#pragma mark - Trigger Events

- (void)onBackButtonTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowMainMenu object:nil];   
}

- (void)onMuteMusicButtonTriggered:(SPEvent*)event {
    [self addChild:unmuteMusicButton];
    [self removeChild:muteMusicButton];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifySetMusicVolume object:[NSNumber numberWithFloat:0]];
}

- (void)onUnmuteMusicButtonTriggered:(SPEvent*)event {
    [self addChild:muteMusicButton];
    [self removeChild:unmuteMusicButton];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifySetMusicVolume object:[NSNumber numberWithFloat:musicVolumeBeforeMute]];
}

- (void)onMuteEffectsButtonTriggered:(SPEvent*)event {
    [self addChild:unmuteEffectsButton];
    [self removeChild:muteEffectsButton];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifySetEffectVolume object:[NSNumber numberWithFloat:0]];
}

- (void)onUnmuteEffectsButtonTriggered:(SPEvent*)event {
    [self addChild:muteEffectsButton];
    [self removeChild:unmuteEffectsButton];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifySetEffectVolume object:[NSNumber numberWithFloat:effectsVolumeBeforeMute]];
}

- (void)onIncMusicVolumeButtonTriggered:(SPEvent*)event {
    musicVolumeBeforeMute += 0.1;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifySetMusicVolume object:[NSNumber numberWithFloat:musicVolumeBeforeMute]];
}

- (void)onDecMusicVolumeButtonTriggered:(SPEvent*)event {
    musicVolumeBeforeMute -= 0.1;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifySetMusicVolume object:[NSNumber numberWithFloat:musicVolumeBeforeMute]];
}

- (void)onIncEffectsVolumeButtonTriggered:(SPEvent*)event {
    effectsVolumeBeforeMute += 0.1;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifySetEffectVolume object:[NSNumber numberWithFloat:effectsVolumeBeforeMute]];
}

- (void)onDecEffectsVolumeButtonTriggered:(SPEvent*)event {
    effectsVolumeBeforeMute -= 0.1;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifySetEffectVolume object:[NSNumber numberWithFloat:effectsVolumeBeforeMute]];
}


#pragma mark - View Management

- (void)viewWillShow {
    if (backButton == nil) {
        SPTextureAtlas *buttonAtlas = [SPTextureAtlas atlasWithContentsOfFile:@"menu_buttons.xml"];
        SPTextureAtlas *playButtonAtlas = [SPTextureAtlas atlasWithContentsOfFile:@"play_buttons.xml"];
        SPTextureAtlas *titleAtlas = [SPTextureAtlas atlasWithContentsOfFile:@"menu_titles.xml"];
        
        backButton = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"backButton"] downState:[buttonAtlas textureByName:@"backButton_down"]];
        backButton.x = 240 - (backButton.width / 2);
        backButton.y = 260;
        
        titleImage = [[SPImage alloc] initWithTexture:[titleAtlas textureByName:@"title_options"]];
        
        //mute
        muteEffectsButton = [[SPButton alloc] initWithUpState:[playButtonAtlas textureByName:@"muteButtonOn"] downState:[playButtonAtlas textureByName:@"muteButtonOn_down"]];
        unmuteEffectsButton = [[SPButton alloc] initWithUpState:[playButtonAtlas textureByName:@"muteButtonOff"] downState:[playButtonAtlas textureByName:@"muteButtonOff_down"]];
        muteMusicButton = [[SPButton alloc] initWithUpState:[playButtonAtlas textureByName:@"muteButtonOn"] downState:[playButtonAtlas textureByName:@"muteButtonOn_down"]];
        unmuteMusicButton = [[SPButton alloc] initWithUpState:[playButtonAtlas textureByName:@"muteButtonOff"] downState:[playButtonAtlas textureByName:@"muteButtonOff_down"]];
        
        //volume change
        incMusicVolumeButton = [[SPButton alloc] initWithUpState:[playButtonAtlas textureByName:@"plusButton"] downState:[playButtonAtlas textureByName:@"plusButton_down"]];
        decMusicVolumeButton = [[SPButton alloc] initWithUpState:[playButtonAtlas textureByName:@"minusButton"] downState:[playButtonAtlas textureByName:@"minusButton_down"]];
        incEffectsVolumeButton = [[SPButton alloc] initWithUpState:[playButtonAtlas textureByName:@"plusButton"] downState:[playButtonAtlas textureByName:@"plusButton_down"]];
        decEffectsVolumeButton = [[SPButton alloc] initWithUpState:[playButtonAtlas textureByName:@"minusButton"] downState:[playButtonAtlas textureByName:@"minusButton_down"]];
        
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.height;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.width;
        
        musicVolumeBeforeMute = MUSIC_VOLUME;
        effectsVolumeBeforeMute = FX_VOLUME;
        
        musicVolumeDesc = [[SPTextField alloc] initWithText:@"Music Volume"];
        musicVolumeDesc.x = 20;
        musicVolumeDesc.y = screenHeight / 2 - 30;
        musicVolumeDesc.height = 30;
        musicVolumeDesc.width = 200;
        musicVolumeDesc.fontName = @"Chalkduster";
        musicVolumeDesc.fontSize = 20;
        musicVolumeDesc.vAlign = SPVAlignTop;
        
        effectsVolumeDesc = [[SPTextField alloc] initWithText:@"Effects_Volume"];
        effectsVolumeDesc.x = 20;
        effectsVolumeDesc.y = screenHeight / 2 + 30;
        effectsVolumeDesc.height = 30;
        effectsVolumeDesc.width = 200;
        effectsVolumeDesc.fontName = @"Chalkduster";
        effectsVolumeDesc.fontSize = 20;
        effectsVolumeDesc.vAlign = SPVAlignTop;
        
        //layout
        //music
        muteMusicButton.x = screenWidth / 2 + 40 - muteMusicButton.width/2;
        muteMusicButton.y = screenHeight / 2 -30;
        
        unmuteMusicButton.x = screenWidth / 2 +40 - unmuteMusicButton.width/2;
        unmuteMusicButton.y = screenHeight / 2 -30;
        
        incMusicVolumeButton.x = screenWidth / 4 * 3 - incMusicVolumeButton.width/2;
        incMusicVolumeButton.y = screenHeight / 2 -30;
        
        decMusicVolumeButton.x = screenWidth / 4 * 3 - decMusicVolumeButton.width/2 + 50;
        decMusicVolumeButton.y = screenHeight / 2 -30;

        //effects
        incEffectsVolumeButton.x = screenWidth / 4 * 3 - incEffectsVolumeButton.width/2;
        incEffectsVolumeButton.y = screenHeight / 2 + 30;
        
        decEffectsVolumeButton.x = screenWidth / 4 * 3 - decEffectsVolumeButton.width/2 + 50;
        decEffectsVolumeButton.y = screenHeight / 2 + 30;
        
        muteEffectsButton.x = screenWidth / 2 + 40 - muteEffectsButton.width/2;
        muteEffectsButton.y = screenHeight / 2 + 30;
        
        unmuteEffectsButton.x = screenWidth / 2 +40 - unmuteEffectsButton.width/2;
        unmuteEffectsButton.y = screenHeight / 2 + 30;
        
        
        // ADD CHILDS
        [self addChild:titleImage];
        [self addChild:backButton];
        [self addChild:effectsVolumeDesc];
        [self addChild:musicVolumeDesc];
        [self addChild:muteMusicButton];
        [self addChild:muteEffectsButton];
        [self addChild:incMusicVolumeButton];
        [self addChild:decMusicVolumeButton];
        [self addChild:incEffectsVolumeButton];
        [self addChild:decEffectsVolumeButton];
        
        // EVENT LISTENERS
        [backButton addEventListener:@selector(onBackButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        
        [muteMusicButton addEventListener:@selector(onMuteMusicButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [unmuteMusicButton addEventListener:@selector(onUnmuteMusicButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [muteEffectsButton addEventListener:@selector(onMuteEffectsButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [unmuteEffectsButton addEventListener:@selector(onUnmuteEffectsButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];

        [incMusicVolumeButton addEventListener:@selector(onIncMusicVolumeButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [decMusicVolumeButton addEventListener:@selector(onDecMusicVolumeButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [incEffectsVolumeButton addEventListener:@selector(onIncEffectsVolumeButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [decEffectsVolumeButton addEventListener:@selector(onDecEffectsVolumeButtonTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    }
}

- (void)viewDidHide {
    [self removeAllChildren];
    self.titleImage = nil;
    self.backButton = nil;
    [musicVolumeDesc release];
    [effectsVolumeDesc release];
    [muteMusicButton release];
    [unmuteMusicButton release];
    [muteEffectsButton release];
    [unmuteEffectsButton release];
    [incMusicVolumeButton release];
    [decMusicVolumeButton release];
    [incEffectsVolumeButton release];
    [decEffectsVolumeButton release];
}


@end
