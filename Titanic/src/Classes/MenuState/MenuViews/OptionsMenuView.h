//
//  OptionsMenuView.h
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"
#import "ViewManagement.h"

#define FX_VOLUME 1.0
#define MUSIC_VOLUME 0.3

@interface OptionsMenuView : SPSprite <ViewManagement> {
    SPButton *backButton;
    SPTextField *musicVolumeDesc, *effectsVolumeDesc;
    SPButton *muteMusicButton, *unmuteMusicButton, *muteEffectsButton, *unmuteEffectsButton;
    SPButton *incMusicVolumeButton, *decMusicVolumeButton, *incEffectsVolumeButton, *decEffectsVolumeButton;
    SPImage *titleImage;
    float musicVolumeBeforeMute, effectsVolumeBeforeMute;
}

@property (nonatomic, strong) SPButton *backButton;
@property (nonatomic, strong) SPImage *titleImage;

@end
