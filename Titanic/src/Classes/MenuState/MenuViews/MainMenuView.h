//
//  MainMenuView.h
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"
#import "ViewManagement.h"

@interface MainMenuView : SPSprite <ViewManagement> {
    SPImage *logo;
    SPButton *playButton;
    SPButton *highscoreButton;
    SPButton *optionsButton;
    SPButton *creditsButton;
}

@property (nonatomic, strong) SPImage *logo;
@property (nonatomic, strong) SPButton *playButton;
@property (nonatomic, strong) SPButton *highscoreButton;
@property (nonatomic, strong) SPButton *optionsButton;
@property (nonatomic, strong) SPButton *creditsButton;

@end
