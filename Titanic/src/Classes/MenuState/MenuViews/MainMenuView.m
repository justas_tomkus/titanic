//
//  MainMenuView.m
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "MainMenuView.h"
#import "GameNotifications.h"

@interface MainMenuView()
- (void)onPlayTriggered:(SPEvent *)event;
- (void)onHighscoreTriggered:(SPEvent *)event;
- (void)onOptionsTriggered:(SPEvent *)event;
- (void)onCreditsTriggered:(SPEvent *)event;
@end

@implementation MainMenuView

@synthesize logo;
@synthesize playButton;
@synthesize highscoreButton;
@synthesize optionsButton;
@synthesize creditsButton;

- (id)init {
    self = [super init];
    if (self) {
        
        
    }
    
    return self;
}

- (void)dealloc
{
    [logo release];
    [playButton release];
    [highscoreButton release];
    [optionsButton release];
    [creditsButton release];
    [super dealloc];
}


#pragma mark - Trigger Events

- (void)onPlayTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowPlayMenu object:[NSNumber numberWithInt:1]];
}

- (void)onHighscoreTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowHighscoreMenu object:[NSNumber numberWithInt:1]];
}

- (void)onOptionsTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowOptionsMenu object:[NSNumber numberWithInt:1]];
}

- (void)onCreditsTriggered:(SPEvent *)event {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyShowCreditsMenu object:[NSNumber numberWithInt:1]];
}


#pragma mark - View Management

- (void)viewWillShow {
    if (logo == nil) {
        logo = [[SPImage alloc] initWithContentsOfFile:@"logo.png"];
        
        SPTextureAtlas *buttonAtlas = [[SPTextureAtlas alloc] initWithContentsOfFile:@"menu_buttons.xml"];
        
        playButton = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"mainButton_1_play"] downState:[buttonAtlas textureByName:@"mainButton_1_play_down"]];
        playButton.x = 300;
        playButton.y = 50;
        
        highscoreButton = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"mainButton_2_highscore"] downState:[buttonAtlas textureByName:@"mainButton_2_highscore_down"]];
        highscoreButton.x = 300;
        highscoreButton.y = 100;
        
        optionsButton = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"mainButton_3_options"] downState:[buttonAtlas textureByName:@"mainButton_3_options_down"]];
        optionsButton.x = 300;
        optionsButton.y = 150;
        
        creditsButton = [[SPButton alloc] initWithUpState:[buttonAtlas textureByName:@"mainButton_4_credits"] downState:[buttonAtlas textureByName:@"mainButton_4_credits_down"]];
        creditsButton.x = 300;
        creditsButton.y = 200;
        
        // ADD CHILDS
        [self addChild:logo];
        [self addChild:playButton];
        [self addChild:highscoreButton];
        [self addChild:optionsButton];
        [self addChild:creditsButton];
        
        [buttonAtlas release];
        
        // EVENT LISTENERS
        [playButton addEventListener:@selector(onPlayTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [highscoreButton addEventListener:@selector(onHighscoreTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [optionsButton addEventListener:@selector(onOptionsTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [creditsButton addEventListener:@selector(onCreditsTriggered:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];    
    }
}

- (void)viewDidHide {
    [self removeAllChildren];
    self.logo = nil;
    self.playButton = nil;
    self.highscoreButton = nil;
    self.optionsButton = nil;
    self.creditsButton = nil;
    
}


@end
