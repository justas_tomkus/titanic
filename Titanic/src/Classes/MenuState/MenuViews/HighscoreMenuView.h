//
//  HighscoreMenuView.h
//  Titanic
//
//  Created by Philipp Anger on 30.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "SPSprite.h"
#import "ViewManagement.h"

@interface HighscoreMenuView : SPSprite <ViewManagement> {
    SPButton *backButton;
    SPImage *titleImage;
}

@property (nonatomic, strong) SPButton *backButton;
@property (nonatomic, strong) SPImage *titleImage;

@end
