//
//  MenuState.h
//  Titanic
//
//  Created by Philipp Anger on 02.05.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameState.h"
#import "MainMenuView.h"
#import "LevelsMenuView.h"
#import "CreditsMenuView.h"
#import "HighscoreMenuView.h"
#import "OptionsMenuView.h"

@interface MenuState : GameState {    
    SPImage *background;
    
    SPSprite<ViewManagement> *currentView;
    MainMenuView *main;
    LevelsMenuView *levels;
    OptionsMenuView *options;
    HighscoreMenuView *highscore;
    CreditsMenuView *credits;
    
    int width;
    int height;
    
    // SOUNDS
    SPSoundChannel *soundChannel;
    
    //Volume
    float musicVolume;
}

@property (nonatomic, strong) SPImage *background;
@property (nonatomic, strong) SPSprite<ViewManagement> *currentView;
@property (nonatomic, strong) MainMenuView *main;
@property (nonatomic, strong) LevelsMenuView *levels;
@property (nonatomic, strong) OptionsMenuView *options;
@property (nonatomic, strong) HighscoreMenuView *highscore;
@property (nonatomic, strong) CreditsMenuView *credits;

- (id)initWithWidth:(float)theWidth andHeight:(float)theHeight;
- (void)showMenu;
@end
