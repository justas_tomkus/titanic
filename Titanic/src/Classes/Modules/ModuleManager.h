//
//  ModuleManager.h
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UpdateableModule.h"
#import "PhysicsModule.h"
#import "TiltModule.h"
#import "LevelModule.h"

@interface ModuleManager : NSObject {
    
    NSMutableArray *updateableModules;
    
    NSDate *previousUpdate;
    
    PhysicsModule *physicsModule;
    TiltModule *tiltModule;
    LevelModule *levelModule;
}

@property (nonatomic, retain) NSDate *previousUpdate;

@property (nonatomic, retain) PhysicsModule *physicsModule;
@property (nonatomic, retain) TiltModule *tiltModule;
@property (nonatomic, retain) LevelModule *levelModule;

+ (ModuleManager *)getInstance;
+ (ModuleManager *)sharedModuleManager;

- (void)addUpdateableModule:(id<UpdateableModule>)module;
- (void)removeUpdateableModule:(id<UpdateableModule>)module;

- (void)update;

@end
