//
//  TiltModule.m
//  Titanic
//
//  Created by MaxL on 28.03.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "TiltModule.h"

@implementation TiltModule

@synthesize currentAcceleration;

- (id)init
{
    self = [super init];
    if (self) {
        alpha = 0.7;
        motionManager = [[CMMotionManager alloc] init]; // motionManager is an instance variable
        motionManager.accelerometerUpdateInterval = 0.03; // 33Hz
        [motionManager startAccelerometerUpdates];
    }
    
    return self;
}

- (void)dealloc {
    if (motionManager.isDeviceMotionActive)
        [motionManager stopDeviceMotionUpdates];
    [motionManager release];
    [super dealloc];
}

#pragma mark - UpdateableModule

- (void)update:(float)deltaTime {
    // alpha is the filter value (instance variable)
    CMAccelerometerData *newestAccel = motionManager.accelerometerData;
    currentAcceleration.x = currentAcceleration.x * (1.0-alpha) + newestAccel.acceleration.x * alpha;
    currentAcceleration.y = currentAcceleration.y * (1.0-alpha) + newestAccel.acceleration.y * alpha;
    currentAcceleration.z = currentAcceleration.z * (1.0-alpha) + newestAccel.acceleration.z * alpha;
}

@end
