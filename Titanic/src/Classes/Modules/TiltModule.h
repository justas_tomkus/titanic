//
//  TiltModule.h
//  Titanic
//
//  Created by MaxL on 28.03.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
#import "UpdateableModule.h"

@interface TiltModule : NSObject <UpdateableModule>
{
    CMMotionManager * motionManager;
    CMAcceleration currentAcceleration;
    double alpha;
}

@property (nonatomic, assign) CMAcceleration currentAcceleration;

@end
