//
//  ModuleManager.m
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "ModuleManager.h"
#import "TiltModule.h"
#import "SynthesizeSingleton.h"


@implementation ModuleManager
SYNTHESIZE_SINGLETON_FOR_CLASS(ModuleManager);

@synthesize previousUpdate;
@synthesize physicsModule;
@synthesize tiltModule;
@synthesize levelModule;

- (id)init
{
    self = [super init];
    if (self) {
        updateableModules = [[NSMutableArray array] retain];
        
        
        physicsModule = [[PhysicsModule alloc] init];
        tiltModule = [[TiltModule alloc] init];
        levelModule = [[LevelModule alloc] init];
        
        [self addUpdateableModule:self.tiltModule];
        [self addUpdateableModule:self.physicsModule];
    }
    return self;
}

- (void)dealloc
{
    [updateableModules release];
    [previousUpdate release];
    [physicsModule release];
    [tiltModule release];
    [levelModule release];
    [super dealloc];
}

- (void)addUpdateableModule:(id<UpdateableModule>)module {
    [updateableModules addObject:module];
}

- (void)removeUpdateableModule:(id<UpdateableModule>)module {
    [updateableModules removeObject:module];
}

- (void)update {
    
    NSDate *curTime = [NSDate date];
    
    float deltaTime = [curTime timeIntervalSinceDate:previousUpdate];
    
    if (deltaTime != deltaTime) {
        deltaTime = 0;
    }
    
    for (id<UpdateableModule> module in updateableModules) {
        [module update:deltaTime];
    }
    
    self.previousUpdate = curTime;
}
@end
