//
//  TMXObject.m
//  Sparrow
//
//  Created by Elliot Franford on 1/20/12.
//  Copyright (c) 2012 Abaondon Hope Games, LLC. All rights reserved.
//

#import "TMXObject.h"

@implementation TMXObject
    @synthesize width = mWidth;
    @synthesize height = mHeight;
    @synthesize x = mX;
    @synthesize y = mY;
    @synthesize gid = mGID;
    @synthesize name = mName;
    @synthesize objType = mType;
    @synthesize properties = mProperties;
    @synthesize polygon = mPolygon;

- (id)init
{
    self = [super init];
    return self;
}

-(id)initWithName:(NSString*)name andType:(NSString*)type{
    self = [super init];
    self.name = [[[NSString alloc]initWithString:name] autorelease];
    self.objType = [[[NSString alloc]initWithString:type] autorelease];
    self.properties = [[[NSMutableDictionary alloc]init] autorelease];
    self.polygon = [[[NSArray alloc] init] autorelease];    
    return self;
}

- (id)initWithName:(NSString*)name andType:(NSString*)type withProperties:(NSMutableDictionary*)props andPoly:(NSArray*)poly
{
    self = [super init];
    self.name = [[[NSString alloc]initWithString:name] autorelease];
    self.objType = [[[NSString alloc]initWithString:type] autorelease];
    self.properties = [[[NSMutableDictionary alloc]initWithDictionary:props] autorelease];
    self.polygon = [[[NSArray alloc] initWithArray:poly] autorelease];    
    return self;
}
- (bool) hitTest:(id) object
{
    bool result = NO;
    return result;
}


-(void) dealloc{
    [mName release];
    [mType release];
    [mProperties release];
    [mPolygon release];
    [super dealloc];
}
@end
