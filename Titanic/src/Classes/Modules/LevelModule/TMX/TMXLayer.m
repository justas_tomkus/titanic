//
//  TMXLayer.m
//  Sparrow
//
//  Created by Elliot Franford on 1/20/12.
//  Copyright (c) 2012 Abaondon Hope Games, LLC. All rights reserved.
//

#import "TMXLayer.h"

@implementation TMXLayer

    @synthesize width = mWidth;
    @synthesize height = mHeight;
    @synthesize x = mX;
    @synthesize y = mY;
    @synthesize name = mName;
    @synthesize properties = mProperties;
    @synthesize data = mData;
    @synthesize visible = mVisible;
    @synthesize opacity = mOpacity;
    @synthesize nsdata = mNSData;
    @synthesize tableData = mTableData;

    @synthesize mapData = mMapData;

    - (id)init
    {
        self = [super init];
        pool = [[NSAutoreleasePool alloc] init];
        mNSData = [[NSMutableData alloc] init];
        return self;
    }
    
    -(id) createTiles
    {
        [self createFromCSV];
        return self;
    }


- (id)createFromCSV{
	mData.data = [mData.data stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
	NSArray *rows = [[[NSArray alloc]initWithArray:[mData.data componentsSeparatedByString:@"\n"]]autorelease];
	int mapHeight = [rows count];
	int mapWidth = [[[rows objectAtIndex:0] componentsSeparatedByString:@","] count];
	
	int amount = ((mapWidth) * (mapHeight)) + mapWidth;
	[mNSData setLength:amount*sizeof(int)];

    mTableData = [[NSMutableArray alloc] initWithCapacity:rows.count];
    for(int y = 0; y < rows.count; y++){
        NSArray *colData  = [[[NSArray alloc]initWithArray:[[rows objectAtIndex:y] componentsSeparatedByString:@","]]autorelease];
        NSMutableArray* colIntData = [[[NSMutableArray alloc] initWithCapacity:colData.count]autorelease];
        for(int x = 0; x < colData.count; x++)
        {
            TMXTile* tile = [[[TMXTile alloc] initWithXYGid:[[colData objectAtIndex:x]intValue] x:x y:y]autorelease];
            [colIntData insertObject:tile atIndex:x];
        }
        [mTableData insertObject:colIntData atIndex:y];
    }
    return self;
}

- (void) dealloc
{
    [pool release];
    [super dealloc];
}
@end
