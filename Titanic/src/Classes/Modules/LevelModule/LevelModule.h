//
//  LevelModule.h
//  Titanic
//
//  Created by MaxL on 26.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Level;

@interface LevelModule : NSObject

- (TMXMap *)loadTMXMapForLevel:(int)levelNumber;

- (void)saveScoreForLevel:(Level *)level;

- (int)getHighestUnlockedLevelNumber;
- (BOOL)levelCompletedSuccessfully:(Level *)level;
- (BOOL)levelCompletedSuccessfullyWithTimeInSeconds:(float)timeInSec maxDust:(int)maxDust collectedDust:(int)colDust limitDustPercentage:(float)limit;

@end
