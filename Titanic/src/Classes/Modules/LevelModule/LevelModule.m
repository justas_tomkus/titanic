//
//  LevelModule.m
//  Titanic
//
//  Created by MaxL on 26.06.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "LevelModule.h"
#import "StorageTool.h"
#import "Level.h"
#import "Constants.h"

@implementation LevelModule

- (TMXMap *)loadTMXMapForLevel:(int)levelNumber {
    TMXMap *map = [[TMXMap alloc] initWithContentsOfFile:[NSString stringWithFormat:@"Level_%d.tmx", levelNumber]];
    return [map autorelease];
}

- (void)saveScoreForLevel:(Level *)level {
    
    int playTimeWorst = level.playTimeAverage * 2;
    int playTimeBest = level.playTimeAverage / 2;
    int playTimeRange = playTimeWorst - playTimeBest;
    int playTime = level.playTimeInSeconds - playTimeWorst;
    
    int dustLimit = level.dustMaximum * level.dustLimitPercentage;
    int dustRange = level.dustMaximum - dustLimit;
    int dustPointsRange = SCORE_DUST_MAXIMUM_POINTS - SCORE_DUST_MINIMUM_POINTS;

    int scoreDust = SCORE_DUST_MINIMUM_POINTS + (dustPointsRange * (level.collectedDust - dustRange));
    int scorePlayTime = SCORE_PLAYTIME_MAXIMUM_POINTS * (1 - (playTime - playTimeRange));
    int scoreDeadMakkus = SCORE_DEAD_MAKKU_POINTS * level.deadMakkusCount;
    int scoreRescuedMakkus = SCORE_RESCUED_MAKKU_POINTS * level.rescuedMakkusCount;
    
    int score = scoreDust + scorePlayTime + scoreRescuedMakkus + scoreDeadMakkus;
    
    [StorageTool saveScore:score forLevel:level.number withTimeInSec:level.playTimeInSeconds maxDust:level.dustMaximum collectedDust:level.collectedDust limitDustPercentage:level.dustLimitPercentage nrOfDeadMakkus:level.deadMakkusCount andNrOfRescuedMakkus:level.rescuedMakkusCount];
}

- (int)getHighestUnlockedLevelNumber {
    NSDictionary *allLvlDict = [StorageTool getScoreForAllLevels];
    
    int doneLvlCount = [allLvlDict count];
    NSDictionary *highestDoneLvl = [allLvlDict objectForKey:[NSString stringWithFormat:@"Level:%d", doneLvlCount]];
    
    float timeInSec = [[highestDoneLvl objectForKey:@"timeInSeconds"] floatValue];
    int maxDust = [[highestDoneLvl objectForKey:@"maximumDust"] intValue];
    int colDust = [[highestDoneLvl objectForKey:@"collectedDust"] intValue];
    float limit = [[highestDoneLvl objectForKey:@"limitDustPercentage"] floatValue];
    
    int highestUnlockedLevel = 1;
    
    if ([self levelCompletedSuccessfullyWithTimeInSeconds:timeInSec maxDust:maxDust collectedDust:colDust limitDustPercentage:limit]) {
        highestUnlockedLevel = doneLvlCount+1;
    }else {
        highestUnlockedLevel = doneLvlCount;
    }
    
    return highestUnlockedLevel;
}

- (BOOL)levelCompletedSuccessfully:(Level *)level {
    return [self levelCompletedSuccessfullyWithTimeInSeconds:level.playTimeInSeconds maxDust:level.dustMaximum collectedDust:level.collectedDust limitDustPercentage:level.dustLimitPercentage];
}

- (BOOL)levelCompletedSuccessfullyWithTimeInSeconds:(float)timeInSec maxDust:(int)maxDust collectedDust:(int)colDust limitDustPercentage:(float)limit {
    return (colDust >= (maxDust)*limit);
}

@end
