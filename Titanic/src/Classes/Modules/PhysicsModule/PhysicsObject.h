//
//  PhysicsBody.h
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhysicsBody.h"

@protocol PhysicsObject <NSObject>

@property (nonatomic, retain) PhysicsBody *body;
@property (nonatomic, assign) CGPoint position;

@end
