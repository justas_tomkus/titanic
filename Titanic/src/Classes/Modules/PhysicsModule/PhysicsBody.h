//
//  PhysicsBody.h
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhysicsForce.h"
#import "Vector2D.h"

//typedef enum {
//    TypeNone,
//    TypeMakku,
//    TypeWall,
//    TypeSofa,
//    TypePit,
//} PhysicsObjectType;

@protocol PhysicsObject;
@class Collider;

@interface PhysicsBody : NSObject {
    
    Collider* owner;
    
    CGPoint position;
    CGFloat angle;
    Vector2D* movementVector;
    Vector2D* ourCollisionChangeMovementVector;
    BOOL moving;
    
    CGFloat mass;
    CGFloat dynamicFriction;
//    PhysicsObjectType type;
    NSMutableSet* currentColliders;
    NSMutableSet* newColliders;
    
    CGFloat staticFriction;
    
    NSMutableArray *currentForces;
    NSMutableArray *currentTorques;
    
}

@property (nonatomic, retain) NSMutableSet* currentColliders;
@property (nonatomic, retain) NSMutableSet* newColliders;
@property (nonatomic, retain) Collider* owner;
@property (nonatomic, retain) Vector2D* movementVector;
@property (nonatomic, retain) Vector2D* ourCollisionChangeMovementVector;
@property (nonatomic, assign) CGFloat mass;
@property (nonatomic, assign) CGFloat dynamicFriction;
@property (nonatomic, assign) CGFloat staticFriction;
@property (nonatomic, assign) CGPoint position;
//@property (nonatomic, assign) PhysicsObjectType type;

- (id)initWithPosition:(CGPoint)thePosition andOwner:(Collider *)theOwner;

- (void)applyForce:(PhysicsForce *)force;
- (void)applyTorque:(Vector2D *)torque;

- (void)resolveCollisionWith:(Collider *)object;

- (void)update:(float)deltaTime;

@end
