//
//  PhysicsForce.h
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vector2D.h"

@interface PhysicsForce : NSObject {
    CGFloat mass;
    Vector2D* acceleration;
}

@property (nonatomic, assign) CGFloat mass;
@property (nonatomic, retain) Vector2D* acceleration;

- (id)initWithMass:(CGFloat)theMass andAcceleration:(Vector2D*)theAcceleration;

+ (PhysicsForce *)forceWithMass:(CGFloat)theMass andAcceleration:(Vector2D*)theAcceleration;

- (CGFloat)getSqrdLength;

@end
