//
//  PhysicsForce.m
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "PhysicsForce.h"

@implementation PhysicsForce

@synthesize mass;
@synthesize acceleration;

- (id)initWithMass:(CGFloat)theMass andAcceleration:(Vector2D*)theAcceleration
{
    self = [super init];
    if (self) {
        self.mass = theMass;
        self.acceleration = theAcceleration;
    }
    return self;
}

+ (PhysicsForce *)forceWithMass:(CGFloat)theMass andAcceleration:(Vector2D*)theDirection {
    return [[[PhysicsForce alloc] initWithMass:theMass andAcceleration:theDirection] autorelease];
}

- (CGFloat)getSqrdLength {
    return mass * (acceleration.x * acceleration.x + acceleration.y+acceleration.y);
}

@end
