//
//  PhysicsModule.h
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpdateableModule.h"
#import "ObjectiveChipmunk.h"



typedef enum {
    TypeNone,
    TypeMakku,
    TypeWall,
    TypeSofa,
    TypePit,
    TypeDust
} PhysicsObjectType;

@interface PhysicsModule : NSObject <UpdateableModule> {
    ChipmunkSpace *space;
    NSMutableArray *shapesAndBodies;
    float dt;
    
}

- (void)resetPhysics;

- (void)addDynamicBody:(ChipmunkBody *)body andShapes:(NSMutableArray *)shapes;
- (void)removeDynamicBody:(ChipmunkBody *)body andShapes:(NSMutableArray *)shapes;
- (void)addStaticBodyWithShapes:(NSMutableArray *)shapes;
- (void)removeStaticBodyWithShapes:(NSMutableArray *)shapes;
- (void)addStaticBodyWithShape:(ChipmunkShape *)shape;
- (void)removeStaticBodyWithShape:(ChipmunkShape *)shape;
- (void)defineSpaceBounds:(CGRect)bounds andFriction:(float)friction;

@end
