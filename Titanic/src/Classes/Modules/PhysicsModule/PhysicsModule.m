//
//  PhysicsModule.m
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "PhysicsModule.h"
#import "ModuleManager.h"
#import "GameNotifications.h"
#import "NSNotificationCenter+UserObject.h"
#import "Makku.h"
#import "Dust.h"
#import "Goal.h"
#import "Light.h"

#define TILT_INTENSITY_FACTOR 500.0

@interface PhysicsModule()
- (BOOL)beginDustCollision:(cpArbiter*)arbiter space:(ChipmunkSpace*)space;
@end

@implementation PhysicsModule

- (id)init {
    self = [super init];
    if (self) {
        [self resetPhysics];
    }
    return self;
}

- (void)dealloc {
    [space release];
    [shapesAndBodies release];
    
    [super dealloc];
}

- (void)resetPhysics {
    [space release];
    space = [[ChipmunkSpace alloc] init];
    
    shapesAndBodies = [[NSMutableArray alloc] init];
    
    // COLLISION HANDLER
    [space addCollisionHandler:self typeA:[Makku class] typeB:[Dust class] begin:@selector(beginDustCollision:space:) preSolve:nil postSolve:nil separate:nil];
    [space addCollisionHandler:self typeA:[Makku class] typeB:[Goal class] begin:@selector(beginGoalCollision:space:) preSolve:nil postSolve:nil separate:nil];
    [space addCollisionHandler:self typeA:[Makku class] typeB:[Light class] begin:nil preSolve:@selector(beginLightCollision:space:) postSolve:nil separate:nil];
}

- (void)addDynamicBody:(ChipmunkBody *)body andShapes:(NSMutableArray *)shapes; {
    for (ChipmunkShape *shape in shapes) {
        [shapesAndBodies addObject:shape];
        [space smartAdd:shape];
    }
    
    [shapesAndBodies addObject:body];
    [space smartAdd:body];
}

- (void)removeDynamicBody:(ChipmunkBody *)body andShapes:(NSMutableArray *)shapes; {
    for (ChipmunkShape *shape in shapes) {
        [shapesAndBodies removeObject:shape];
        [space smartRemove:shape];
    }
    
    [shapesAndBodies removeObject:body];
    [space smartRemove:body];
}

- (void)addStaticBodyWithShapes:(NSMutableArray *)shapes {
    for (ChipmunkShape *shape in shapes) {
        [shapesAndBodies addObject:shape];
        [space smartAdd:shape];
    }
}

- (void)removeStaticBodyWithShapes:(NSMutableArray *)shapes {
    for (ChipmunkShape *shape in shapes) {
        [shapesAndBodies removeObject:shape];
        [space smartRemove:shape];
    }
}

- (void)addStaticBodyWithShape:(ChipmunkShape *)shape {
    [shapesAndBodies addObject:shape];
    [space smartAdd:shape];
}

- (void)removeStaticBodyWithShape:(ChipmunkShape *)shape {
    [shapesAndBodies removeObject:shape];
    [space smartRemove:shape];
}

- (void)defineSpaceBounds:(CGRect)bounds andFriction:(float)friction {
    [space addBounds:bounds
           thickness:100.0f
          elasticity:0.7f friction:friction
              layers:CP_ALL_LAYERS group:CP_NO_GROUP
       collisionType:(id)TypeWall
     ];
}


#pragma mark - Collision Handling

- (BOOL)beginDustCollision:(cpArbiter*)arbiter space:(ChipmunkSpace*)space {
    
    CHIPMUNK_ARBITER_GET_SHAPES(arbiter, makkuShape, dustShape);

    Makku *makku = makkuShape.data;
    Dust *dust = dustShape.data;
    
    if (dust != nil) {
        if (!dust.collected) {
            [makku collectedDust:dust];
            [dust wasCollected];
            dustShape.data = nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyDustCollected object:nil];
        }
    }
    
    return NO;
}

- (BOOL)beginGoalCollision:(cpArbiter*)arbiter space:(ChipmunkSpace*)space {
    
    CHIPMUNK_ARBITER_GET_SHAPES(arbiter, makkuShape, goalShape);
    
    Makku *makku = makkuShape.data;
    Goal *goal = goalShape.data;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyGoalCollision object:nil userInfo:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:makku,goal,nil] forKeys:[NSArray arrayWithObjects:@"makku",@"goal", nil]]];
    return NO;
}

- (BOOL)beginLightCollision:(cpArbiter*)arbiter space:(ChipmunkSpace*)space {
    //NSLog(@"light col");
    CHIPMUNK_ARBITER_GET_SHAPES(arbiter, makkuShape, lightShape);
    
    Makku *makku = makkuShape.data;
    Light *light = lightShape.data;
    
    if (light != nil && [light isKindOfClass:[Light class]]) {
        [light applyLightDamage:makku withDeltaTime:dt];
    }
    
    return NO;
}

#pragma mark - UpdateableModule

- (void)update:(float)deltaTime {
    deltaTime = fmin(0.06, deltaTime);
    dt = deltaTime;
    [space step:deltaTime];
    
    
    space.gravity = cpvmult(cpv(-([ModuleManager sharedModuleManager].tiltModule.currentAcceleration.y), -([ModuleManager sharedModuleManager].tiltModule.currentAcceleration.x)), TILT_INTENSITY_FACTOR);
}

@end
