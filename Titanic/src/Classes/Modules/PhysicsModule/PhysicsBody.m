//
//  PhysicsBody.m
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "PhysicsBody.h"
#import "PhysicsObject.h"
#import "Collider.h"
#import "Vector2D.h"

#define DAMPENING           2.0

#define SCALE 20.0
#define GRAVITY 9.81

//BOOL isZero(float a)
//{
//    return (fabs(a) < kEpsilon);
//}

float onLine(float x1, float y1, float x2, float y2, Vector2D* p) {
    float tmpX = x1-x2;
    float tmpY = y1-y2;
    float alpha;
    if (!isZero(tmpX)) {
        alpha = (p.x - x2)/tmpX;
    } else if(!isZero(tmpY)) {
        alpha = (p.y - y2)/tmpY;
    } else {
        alpha = 42;
//        NSLog(@"Idiot! Ask Andy and Justas!");
    }
    return alpha;
}

@interface PhysicsBody()
- (CGFloat)getLength:(CGPoint)vector;
@end

@implementation PhysicsBody
@synthesize currentColliders;
@synthesize newColliders;
@synthesize owner;
@synthesize mass;
@synthesize movementVector;
@synthesize dynamicFriction;
@synthesize staticFriction;
@synthesize position;
//@synthesize type;
@synthesize ourCollisionChangeMovementVector;


- (id)initWithPosition:(CGPoint)thePosition andOwner:(Collider *)theOwner
{
    self = [super init];
    if (self) {
        self.position = CGPointMake(thePosition.x, thePosition.y);
        self.mass = 1;
        self.dynamicFriction = 0.5;
        self.owner = theOwner;
//        type = TypeNone;
        
        currentForces = [[NSMutableArray alloc] init];
        currentTorques = [[NSMutableArray alloc] init];
        
        currentColliders = [[NSMutableSet  alloc ] init];
        newColliders = [[NSMutableSet alloc] init];
        
        movementVector = [Vector2D newWithX:0 Y:0];
        ourCollisionChangeMovementVector  = [Vector2D newWithX:0 Y:0];
        moving = NO;
    }
    return self;
}

- (void)dealloc
{
    [currentForces release];
    [currentTorques release];
    
    [currentColliders release];
    [movementVector release];
    [super dealloc];
}

#pragma mark - Private Setters

//- (void)setType:(PhysicsObjectType)theType {
//    if (self.type == TypeNone) {
//        type = theType;
//    }
//}

#pragma mark - Methods

- (void)applyForce:(PhysicsForce *)force {
    [currentForces addObject:force];
}

- (void)applyTorque:(Vector2D *)torque {
    [currentTorques addObject:torque];
}

- (void)resolveCollisionWith:(Collider *)object {
//    NSLog(@"COLLISION !!");
    //[newColliders addObject:object];
    
    // if we weren't colliding with this object before
    if (![currentColliders containsObject:object]) {
        
        if (self.owner.type == ShapeTypeCircle) {
            if (object.type == ShapeTypeCircle) {
                Vector2D* ourMoveMoment = [movementVector cmult:mass];
                Vector2D* otherMoveMoment = [object.body.movementVector cmult:object.body.mass];
                Vector2D* reflectVector = [[ourMoveMoment cadd:otherMoveMoment] normalized];
                
                Vector2D* reflectNormal = [reflectVector perp];
                
                Vector2D* result = [ourMoveMoment csub:([[reflectNormal cmult:2] cmult:([ourMoveMoment dot:reflectNormal] )])];
                [ourCollisionChangeMovementVector add:[result cdiv:mass]];
            } else {
               // object.type == ShapeTypeRectangle
                float halfWidth = object.width / 2;
                float halfHeight = object.height / 2;
                float centerX = object.body.position.x + object.width/2;
                float centerY = object.body.position.y + object.height/2;
                centerX = object.x + object.width/2;
                centerY = object.y + object.height/2;

                Vector2D* leftTop = [Vector2D withX:(centerX - halfWidth) Y:(centerY + halfHeight)];
                Vector2D* leftBottom = [Vector2D withX:(centerX - halfWidth) Y:(centerY - halfHeight)];
                Vector2D* rightTop = [Vector2D withX:(centerX + halfWidth) Y:(centerY + halfHeight)];
                Vector2D* rightBottom = [Vector2D withX:(centerX + halfWidth) Y:(centerY - halfHeight)];
                
                Vector2D* left = [leftTop csub:leftBottom];
                Vector2D* bottom = [leftBottom csub:rightBottom];
                Vector2D* right = [rightBottom csub:rightTop];
                Vector2D* top = [rightTop csub:leftTop];
//                NSLog(@"top: %f, %f", top.x, top.y);
//                NSLog(@"left: %f, %f", left.x, left.y);
//                NSLog(@"bottom: %f, %f", bottom.x, bottom.y);
//                NSLog(@"right: %f, %f", right.x, right.y);
                
                Vector2D* circle_pos = [Vector2D withX:self.position.x Y:self.position.y];
                
                Vector2D *result_v= [Vector2D zero];
                
                Vector2D* arrEnd[] = {leftTop, leftBottom, rightBottom, rightTop};
                Vector2D* arrBegin[] = {leftBottom, rightBottom, rightTop, leftTop};
                Vector2D* arr[] = {left, bottom, right, top};
                
                for (int i=0; i < 4; i++) {
                    Vector2D *pt_v = [circle_pos csub: arrBegin[i]];
                    //project pt_v on arr[i]
                    Vector2D *seg_v_norm = [arr[i] normalized];
                    Vector2D *closest;
                    float proj_v_len = [pt_v dot:seg_v_norm];
                    if (proj_v_len < 0) {
                        //out of line
                        closest = arrBegin[i];
                    } else if (proj_v_len > [arr[i] length]) {
                        //out of line
                        closest = arrEnd[i];
                    } else {
                        Vector2D *proj_v = [seg_v_norm cmult:proj_v_len];
                        closest = [arrBegin[i] cadd:proj_v];
                    }
                    Vector2D *dist_v = [circle_pos csub:closest];
                    float radius = self.owner.radius;
                    float distance = [dist_v length];
//                    NSLog(@"radius: %f, distance: %f", radius, distance);

                    float specialVal = [movementVector dot:[dist_v cdiv: distance]];
                    if (specialVal > 0)
                        continue;
                    if (distance < radius) {
                        //float offset = (radius - distance) * [dist_v normalized] cm;
                        [result_v add:arr[i]];
                    }
                }
//                //Vector2D* velocity = [movementVector mult:10000000000.0f];
//                float cons = 100000.0f;
//                Vector2D* velocity = [[movementVector normalized] cmult:cons];
//                
//                // common to every calc
//                float xy1 = self.position.x*(self.position.y + velocity.y) - self.position.y*(self.position.x + velocity.x);
//                
//                Vector2D* endRayPoint = [[Vector2D withX:self.position.x Y:self.position.y] cadd:velocity];
//                float dist = 100000.0f;
//                Vector2D* result = nil;
//                
//                
//                
//                // left wall
//                float nom = [velocity cross: left];
//                float xy3, tmpx, tmpy;
//                Vector2D *leftp = nil, *bottomp = nil, *rightp = nil, *topp = nil;
//                if (isZero(nom) != YES) {
//                    xy3 = [leftTop cross:leftBottom];
//                    tmpx = (xy1*left.x - xy3*velocity.x) / nom;
//                    tmpy = (xy1*left.y - xy3*velocity.y) / nom;
//                    leftp = [Vector2D withX:tmpx Y:tmpy];
//                    float alpha = onLine(makkuCenter.x, makkuCenter.y, endRayPoint.x, endRayPoint.y, leftp);
//                    if (alpha < dist && alpha >= 0) {
//                        dist = alpha;
//                        result = left;
//                    }
//                }
//                
//                // bottom wall
//                nom = [velocity cross:bottom];
//                if (isZero(nom) != YES) {
//                    xy3 = [leftBottom cross:rightBottom];
//                    tmpx = (xy1*bottom.x - xy3*velocity.x) / nom;
//                    tmpy = (xy1*bottom.y - xy3*velocity.y) / nom;
//                    bottomp = [Vector2D withX:tmpx Y:tmpy];
//                    float alpha = onLine(makkuCenter.x, makkuCenter.y, endRayPoint.x, endRayPoint.y, bottomp);
//                    if (alpha < dist && alpha >= 0) {
//                        dist = alpha;
//                        result = bottom;
//                    }
//                }
//                
//                // right wall
//                nom = [velocity cross:right];
//                if (isZero(nom) != YES) {
//                    xy3 = [rightBottom cross:rightTop];
//                    tmpx = (xy1*right.x - xy3*velocity.x) / nom;
//                    tmpy = (xy1*right.y - xy3*velocity.y) / nom;
//                    rightp = [Vector2D withX:tmpx Y:tmpy];
//                    float alpha = onLine(makkuCenter.x, makkuCenter.y, endRayPoint.x, endRayPoint.y, rightp);
//                    if (alpha < dist && alpha >= 0) {
//                        dist = alpha;
//                        result = right;
//                    }            
//                }
//                
//                // top wall
//                nom = [velocity cross:top];
//                if (isZero(nom) != YES) {
//                    xy3 = [rightTop cross:leftTop];
//                    tmpx = (xy1*top.x - xy3*velocity.x) / nom;
//                    tmpy = (xy1*top.y - xy3*velocity.y) / nom;
//                    topp = [Vector2D withX:tmpx Y:tmpy];
//                    float alpha = onLine(makkuCenter.x, makkuCenter.y, endRayPoint.x, endRayPoint.y, topp);
//                    if (alpha < dist && alpha >= 0) {
//                        dist = alpha;
//                        result = top;
//                    }
//                }
                
                if ([result_v isZero])
                    return;
                
                // now we can use topp, rightp, bottomp and leftp
//                NSLog(@"Result colliding surface: %f, %f", result_v.x, result_v.y);
                result_v = [[[result_v perp] normalized] cmult:1];
                result_v = [movementVector csub:([[result_v cmult:2] cmult:([movementVector dot:result_v] )])];
//                NSLog(@"MoveVec: %f, %f", movementVector.x, movementVector.y);
//                NSLog(@"Changed MoveVec: %f, %f", result_v.x, result_v.y);
                [ourCollisionChangeMovementVector add:result_v];
                
            }
        }
    } // END: if we are colliding with this object
}

- (void)update:(float)deltaTime {
//    if (self.type != TypeMakku) {
//        return;
//    }
                                 
//    if (![ourCollisionChangeMovementVector isZero])
    BOOL skip_force = NO;
    if (ourCollisionChangeMovementVector.x !=0 && ourCollisionChangeMovementVector.y != 0) {
        float speed = [movementVector length];
        [movementVector setX:ourCollisionChangeMovementVector.x];
        [movementVector setY:ourCollisionChangeMovementVector.y];
        [movementVector normalize];
        [movementVector mult:speed];
//        movementVector = [ourCollisionChangeMovementVector copy] ;
//        movementVector = ourCollisionChangeMovementVector;
        [ourCollisionChangeMovementVector zero];
        skip_force = YES;
    }    
    
    CGFloat x = 0;
    CGFloat y = 0;
    if (true || !skip_force) {
        for (PhysicsForce *force in currentForces) {
            x += force.acceleration.x * force.mass;
            y += force.acceleration.y * force.mass;
        }
    
        [currentForces removeAllObjects];
    
    
        Vector2D *newMovement = [Vector2D withX:x*SCALE Y:y*SCALE];
        
        //add summed new forces to the current moving force
        movementVector.x += (newMovement.x);// * deltaTime;
        movementVector.y += (newMovement.y);// * deltaTime;
    
        float forceX = 0;
        float forceY = 0;
        
        PhysicsForce* normalForce = [PhysicsForce forceWithMass:(mass) andAcceleration:[Vector2D withX:0 Y:GRAVITY]];
        //if not NaN
        if (movementVector.x == movementVector.x) {
            forceX = dynamicFriction * normalForce.mass * movementVector.x/movementVector.length * GRAVITY;
            forceY = dynamicFriction * normalForce.mass * movementVector.y/movementVector.length * GRAVITY;
        }
        
        //check if static friction force is reached
        if (!moving) {
            if (normalForce.getSqrdLength * staticFriction < movementVector.lengthSquared)
                moving = YES;
        }
        
        //check if dynamic friction should be used to decrease the actual moving force
        if (moving) {
            // if moving force is less than the friction force the object stops, else decrease moving force
            if (movementVector.lengthSquared <= normalForce.getSqrdLength * dynamicFriction) {
                movementVector.x = 0;
                movementVector.y = 0;
                moving = NO;
            } else {
                movementVector.x -= forceX;
                movementVector.y -= forceY;
            }
        }
    }
    
    //apply moving force to change position
    position.x += movementVector.x * deltaTime;
    position.y += movementVector.y * deltaTime;
    
    self.owner.position = position;
    
//    for (PhysicsForce *force in currentTorques) {
//        
//    }
    
}

- (void)setMass:(CGFloat)theMass {
    if (theMass < 0.01) {
        mass = 0.01;
    }else if(theMass > 100 ) {
        mass = 100;
    }else {
        mass = theMass;
    }
}

#pragma mark- private methods

- (CGFloat)getLength:(CGPoint)vector {
    return sqrt((vector.x*vector.x) + (vector.y*vector.y));
}


@end
