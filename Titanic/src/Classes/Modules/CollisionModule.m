//
//  CollisionModule.m
//  Titanic
//
//  Created by Philipp Anger on 16.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import "CollisionModule.h"
#import "ModuleManager.h"
#import "Wall.h"
#import "Couch.h"

@implementation CollisionModule

@synthesize makkusList;
@synthesize colliderList;

- (id)init
{
    self = [super init];
    if (self) {
        makkusList = [[NSMutableArray alloc] init];
        colliderList = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [makkusList release];
    [colliderList release];
    [super dealloc];
}

-(void)registerCollidableObject:(Collider *)object
{
    if([object isKindOfClass:[Makku class]]) {
        [makkusList addObject:object];
    } else {
        [colliderList addObject:object];
    }
}

-(void)unregisterCollidableObject:(Collider *)object
{
    if([object isKindOfClass:[Makku class]]) {
        [makkusList removeObject:object];
    } else {
        [colliderList removeObject:object];
    }
}


-(void)update:(float)deltaTime {
    
#warning TODO: should obstacles also be checked to collide each other?
    for (Makku *makku in makkusList) {
        makku.body.currentColliders = [makku.body.newColliders copy];
        [makku.body.newColliders removeAllObjects];
    }
    
    for (Makku *makku in makkusList) { 
        
        for (Collider *collider in colliderList) {
            if([self isCircle:makku collidingWithRectangle:collider]) {
                
                if([collider hasChildColliders]) {
                    for(Collider *childCollider in [collider getChildColliders]) {
                        if (childCollider.type == ShapeTypeRectangle && [self isCircle:makku collidingWithRectangle:childCollider]) {
                                [[ModuleManager sharedModuleManager].physicsModule resolveCollisionOf:makku andOf:childCollider];
                        } else if (childCollider.type == ShapeTypeCircle && [self isCircle:makku collidingWithCircle:childCollider]) {
                                [[ModuleManager sharedModuleManager].physicsModule resolveCollisionOf:makku andOf:childCollider];
                        }
                    }
                    
                } else {
                    //adding to temp
                    [[ModuleManager sharedModuleManager].physicsModule resolveCollisionOf:makku andOf:collider];   
                }
            }
        }
        
        for (Makku *tmpMakku in makkusList) {
            if([self isCircle:makku collidingWithRectangle:tmpMakku] && ![tmpMakku isEqual:makku]) {
                [[ModuleManager sharedModuleManager].physicsModule resolveCollisionOf:makku andOf:tmpMakku];
            }
        }
    }
}

-(BOOL)isCircle:(Collider *)circleCollider collidingWithRectangle:(Collider *)rectangleCollider {
    float circleDistanceX = abs(circleCollider.x - rectangleCollider.x - rectangleCollider.width/2);
    float circleDistanceY = abs(circleCollider.y - rectangleCollider.y - rectangleCollider.height/2);
    
    if (circleDistanceX > (rectangleCollider.width/2 + circleCollider.radius)) { return false; }
    if (circleDistanceY > (rectangleCollider.height/2 + circleCollider.radius)) { return false; }
    
    if (circleDistanceX <= (rectangleCollider.width/2)) { return true; } 
    if (circleDistanceY <= (rectangleCollider.height/2)) { return true; }
    
    float cornerDistanceSquare = powf(circleDistanceX - rectangleCollider.width/2,2) + powf(circleDistanceY - rectangleCollider.height/2,2);
    
    return (cornerDistanceSquare <= powf(circleCollider.radius,2));
}

-(BOOL)isCircle:(Collider *)collider1 collidingWithCircle:(Collider *)collider2 {
    SPPoint *p1 = [SPPoint pointWithX:collider1.x y:collider1.y];
    SPPoint *p2 = [SPPoint pointWithX:collider2.x y:collider2.y];
    float distance = [SPPoint distanceFromPoint:p1 toPoint:p2];
    
    if (distance < collider1.radius + collider2.radius) {
        return YES;
    }

    return NO;
}


@end
