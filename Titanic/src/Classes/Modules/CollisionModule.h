//
//  CollisionModule.h
//  Titanic
//
//  Created by Philipp Anger on 16.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpdateableModule.h"
#import "Makku.h"
#import "Collider.h"

@interface CollisionModule : NSObject <UpdateableModule> {
    NSMutableArray *makkusList;
    NSMutableArray *colliderList;
    
}

@property (nonatomic,retain) NSMutableArray *makkusList;
@property (nonatomic,retain) NSMutableArray *colliderList;

-(void)registerCollidableObject:(Collider *)object;
-(void)unregisterCollidableObject:(Collider *)object;

-(BOOL)isCircle:(Collider *)makku collidingWithRectangle:(Collider *)collider;
-(BOOL)isCircle:(Collider *)makku1 collidingWithCircle:(Collider *)makku2;


@end
