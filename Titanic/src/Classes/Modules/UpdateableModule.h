//
//  UpdateableModule.h
//  Titanic
//
//  Created by MaxL on 11.04.12.
//  Copyright (c) 2012 IM11 - Game Production - FH Hageberg. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpdateableModule <NSObject>
@required
- (void)update:(float)deltaTime;
@end
